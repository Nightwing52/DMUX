# The ZLIB license
#
# Copyright (c) 2015 Andr� Netzeband
# Copyright (c) 2016 Brigham Keys, Esq.
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
set(CLIENT_SOURCE_FILES
#  src/client/Client.cpp
  src/client/Game.cpp
  src/client/JukeBox.cpp
  src/client/Main.cpp
  src/client/Player.cpp
  )

set(SERVER_SOURCE_FILES
  src/server/ServerMain.cpp
  src/server/Server.cpp
  )

set(SYS_SOURCE_FILES
  src/sys/World.cpp
  src/sys/Graphics.cpp
  src/sys/TriMesh.cpp
  src/sys/Physics.cpp
  src/sys/XmlParser.cpp
  )

set(COMMON_SOURCE_FILES
  src/observable/obs/connection.cpp
  src/DmuxCommon.cpp
  src/PacketTypes.cpp
  src/Simulation.cpp
  src/Arena.cpp
  src/Observer.cpp
  src/Vehicle.cpp
  src/Layout.cpp
  src/NetCommon.cpp
  src/NetworkQueue.cpp
  )

set(COMPONENT_SOURCE_FILES
  src/comp/World.cpp
  src/comp/Graphics.cpp
  src/comp/Physics.cpp
  src/comp/World.cpp
  )

set(GUI_SOURCE_FILES
  src/gui/Menu.cpp
  src/gui/Settings.cpp
  src/gui/Gui.cpp
  src/gui/Garage.cpp
  )

set(ALL_CLIENT_FILES
  ${CLIENT_SOURCE_FILES}
  ${GUI_SOURCE_FILES}
  )

set(ALL_SERVER_FILES
  ${SERVER_SOURCE_FILES}
  )

if(REMOTERY)
    set(REMOTERY_SOURCE
        src/remotery/Remotery.c)
endif()

add_library(core STATIC
  ${COMMON_SOURCE_FILES}
  ${COMPONENT_SOURCE_FILES}
  ${SYS_SOURCE_FILES}
  ${REMOTERY_SOURCE}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  core
  )

include_directories(
  SYSTEM include/
  SYSTEM include/cereal/
  SYSTEM include/easyloggingpp/src/
  SYSTEM src/observable/
  src/
  )
