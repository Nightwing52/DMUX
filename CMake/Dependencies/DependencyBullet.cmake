find_package(Bullet)

if(WIN32)
  set(BULLET_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/src")
  set(BULLET_LIBRARIES
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBullet2FileLoader.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBullet3Collision.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBullet3Common.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBullet3Dynamics.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBullet3Geometry.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBullet3OpenCL_clew.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBulletCollision.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBulletDynamics.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBulletFileLoader.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBulletInverseDynamics.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBulletInverseDynamicsUtils.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBulletSoftBody.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBulletWorldImporter.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libBulletXmlWorldImporter.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libConvexDecomposition.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libGIMPACTUtils.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libHACD.a"
    "${CMAKE_SOURCE_DIR}/lib-woe32/bullet3/build/lib/libLinearMath.a"
    )
endif()

include_directories(
  SYSTEM ${BULLET_INCLUDE_DIR}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${BULLET_LIBRARIES}
  )
