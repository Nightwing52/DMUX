find_package(OpenAL)


if(WIN32)
  set(CAUDIO_LIBRARY "${CMAKE_SOURCE_DIR}/lib-woe32/cAudio/build/cAudio/libcAudio.a")
  set(CAUDIO_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/cAudio/build/include/")
  set(CAUDIO_CONFIG_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/cAudio/cAudio/include/")

  set(OPENAL_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/cAudio/Dependencies/include/")
  set(OPENAL_LIBRARY "${CMAKE_SOURCE_DIR}/lib-woe32/cAudio/Dependencies/bin/release/OpenAL32.dll")

set(CAUDIO_LIBRARY
  ${CAUDIO_LIBRARY}
  "${CMAKE_SOURCE_DIR}/lib-woe32/cAudio/build/DependenciesSource/libvorbis-1.3.2/libVorbis.a"
  "${CMAKE_SOURCE_DIR}/lib-woe32/cAudio/build/DependenciesSource/libogg-1.2.2/libOgg.a"
  )

elseif(UNIX AND NOT APPLE)
  set(CAUDIO_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-gnu-linux/cAudio-master/build/include/")
  set(CAUDIO_CONFIG_DIR "${CMAKE_SOURCE_DIR}/lib-gnu-linux/cAudio-master/cAudio/include/")

set(CAUDIO_LIBRARY
  ${CAUDIO_LIBRARY}
  "${CMAKE_SOURCE_DIR}/lib-gnu-linux/cAudio-master/build/cAudio/libcAudio.a"
  "${CMAKE_SOURCE_DIR}/lib-gnu-linux/cAudio-master/build/DependenciesSource/libvorbis-1.3.2/libVorbis.a"
  "${CMAKE_SOURCE_DIR}/lib-gnu-linux/cAudio-master/build/DependenciesSource/libogg-1.2.2/libOgg.a"
  )
endif()


include_directories(
  SYSTEM ${CAUDIO_INCLUDE_DIR}
  SYSTEM ${CAUDIO_CONFIG_DIR}
  SYSTEM ${OPENAL_INCLUDE_DIR}
  )

set(CAUDIO_LIBRARY
  ${CAUDIO_LIBRARY}
  ${OPENAL_LIBRARY}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${CAUDIO_LIBRARY}
  )
