if(WIN32)

set(IRRIMGUI_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/IrrIMGUI-0.3.1/includes/")
set(IMGUI_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/IrrIMGUI-0.3.1/dependency/")
set(IRRIMGUI_LIBRARY "${CMAKE_SOURCE_DIR}/lib-woe32/IrrIMGUI-0.3.1/build/libIrrIMGUI.a")
elseif(UNIX AND NOT APPLE)
set(IRRIMGUI_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-gnu-linux/IrrIMGUI-master/includes/")
set(IMGUI_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-gnu-linux/IrrIMGUI-master/dependency/")
set(IRRIMGUI_LIBRARY "${CMAKE_SOURCE_DIR}/lib-gnu-linux/IrrIMGUI-master/build/libIrrIMGUI.so")
endif()

include_directories(
  SYSTEM ${IRRIMGUI_INCLUDE_DIR}
  SYSTEM ${IMGUI_INCLUDE_DIR}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${IRRIMGUI_LIBRARY}
  )
