find_package(OpenGL)
find_package(Irrlicht)

if(WIN32)
  set(IRRLICHT_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/IrrlichtCMake-0.2.2/include")
  set(IRRLICHT_LIBRARY "${CMAKE_SOURCE_DIR}/lib-woe32/IrrlichtCMake-0.2.2/build/libIrrlicht.a")
endif()

include_directories(
  SYSTEM ${IRRLICHT_INCLUDE_DIR}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${IRRLICHT_LIBRARY}
  )
