if(WIN32)
  set(PLUTO_LIBRARY "${CMAKE_SOURCE_DIR}/lib-woe32/pluto/build/libpluto.a")
  set(PLUTO_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/pluto/include/")
elseif(UNIX AND NOT APPLE)
  set(PLUTO_LIBRARY "${CMAKE_SOURCE_DIR}/lib-gnu-linux/pluto/build/libpluto.a")
  set(PLUTO_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-gnu-linux/pluto/include/")
endif()

include_directories(
  SYSTEM ${PLUTO_INCLUDE_DIR}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${PLUTO_LIBRARY}
  )
