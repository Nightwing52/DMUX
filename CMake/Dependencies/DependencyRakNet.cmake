if(WIN32)
  set(RAKNET_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/RakNet/include/")
  set(RAKNET_LIBRARY "${CMAKE_SOURCE_DIR}/lib-woe32/RakNet/build/libRakNet.a")
elseif(UNIX AND NOT APPLE)
  set(RAKNET_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-gnu-linux/RakNet-master/include") # We need to use our fork of RakNet where possible
  set(RAKNET_LIBRARY "${CMAKE_SOURCE_DIR}/lib-gnu-linux/RakNet-master/build/libRakNet.a")
endif()

include_directories(SYSTEM ${RAKNET_INCLUDE_DIR})

set(DMUX_DEPENDENCY_LIBRARIES
    ${DMUX_DEPENDENCY_LIBRARIES}
    ${RAKNET_LIBRARY}
    )
