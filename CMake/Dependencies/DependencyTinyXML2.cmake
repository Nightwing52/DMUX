find_package(TinyXML2)

if(WIN32)
  set(TINYXML2_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/lib-woe32/tinyxml2-4.0.1/")
  set(TINYXML2_LIBRARY "${CMAKE_SOURCE_DIR}/lib-woe32/tinyxml2-4.0.1/build/libtinyxml2.a")
endif()

include_directories(
  SYSTEM ${TINYXML2_INCLUDE_DIR}
  )

set(DMUX_DEPENDENCY_LIBRARIES
  ${DMUX_DEPENDENCY_LIBRARIES}
  ${TINYXML2_LIBRARY}
  )
