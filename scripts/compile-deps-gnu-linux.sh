#!/usr/bin/env bash
: '
    copyright (c) 2016 Brigham Keys, Esq.
    This script is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'
cd ..
set -e
mkdir -p lib-gnu-linux/
cd lib-gnu-linux/

function installed {
    echo "$1 dep is installed. If it needs updating remove the directory and run this script again."
}

# cAudio:
if [ ! -d "cAudio-master" ] ; then
    wget https://github.com/ComradeKeys/cAudio/archive/master.zip
    unzip master.zip
    rm master.zip
    cd cAudio-master/
    mkdir -p build
    cd build/
    cmake -DCAUDIO_STATIC=TRUE -DCAUDIO_BUILD_SAMPLES=FALSE ..
    make
    cd ../../
else
    installed "cAudio"
fi

# Pluto:
if [ ! -d "pluto/" ] ; then
    git clone --depth=1 https://gitlab.com/CollectiveTyranny/pluto.git
    cd pluto/
    rm -rf .git/
    mkdir build/
    cd build/
    cmake ..
    make
    cd ../../
else
    installed "Pluto"
fi

# CPPLocate:
if [ ! -d "cpplocate-master" ] ; then
    wget https://github.com/cginternals/cpplocate/archive/master.zip
    unzip master.zip
    rm master.zip
    cd cpplocate-master/
    mkdir build
    cd build/
    cmake ..
    make
    cd ../../
else
    installed "CPPLocate"
fi

# RakNet:
if [ ! -d "RakNet-master" ] ; then
    wget https://github.com/ComradeKeys/RakNet/archive/master.zip
    unzip master.zip
    rm master.zip
    cd RakNet-master/
    mkdir build/
    cd build/
    cmake ..
    make
    cd ../../
else
    installed "RakNet"
fi

if [ ! -d "IrrIMGUI-master/" ] ; then
    wget https://github.com/Deathsbreed/IrrIMGUI/archive/master.zip
    unzip master.zip
    rm master.zip
    cd IrrIMGUI-master/
    cd dependency/
    rm -rf IMGUI/
    git clone --depth=1 https://github.com/ocornut/imgui.git
    mv imgui/ IMGUI/
    cd ../
    mkdir build
    cd build
    cmake .. -DIRRIMGUI_BUILD_EXAMPLES=OFF
    make
else
    installed "IrrIMGUI"
fi
