#!/bin/bash

:'
cd ..
mkdir lib-woe32/

cd lib-woe32/
git clone --depth=1 https://github.com/ComradeKeys/cAudio.git
cd cAudio/
mkdir -p build
cd build/
#For some reason cAudio will only cross compile if we run this twice
rm -rf *
cmake ../ -DCAUDIO_DEPENDENCIES_DIR=../Dependencies64/  -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake -DCAUDIO_BUILD_SAMPLES=FALSE -DOPENAL_INCLUDE_DIR=../Dependencies64/include/ -DOPENAL_LIBRARY_REL=../Dependencies64/bin/release/OpenAL32.dll -DOPENAL_LIBRARY_DBG=../Dependencies64/bin/debug/OpenAL32.dll -DCAUDIO_STATIC=TRUE
cmake ../ -DCAUDIO_DEPENDENCIES_DIR=../Dependencies64/  -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake -DCAUDIO_BUILD_SAMPLES=FALSE -DOPENAL_INCLUDE_DIR=../Dependencies64/include/ -DOPENAL_LIBRARY_REL=../Dependencies64/bin/release/OpenAL32.dll -DOPENAL_LIBRARY_DBG=../Dependencies64/bin/debug/OpenAL32.dll -DCAUDIO_STATIC=TRUE
set -e #stop if we get errors, rest of script should be error free

make 
cd ../../

git clone --depth=1 https://github.com/ComradeKeys/RakNet.git
cd RakNet/
mkdir -p build/
cd build/
rm -rf *
cmake ../ -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake
make 
cd ../../

wget https://github.com/leethomason/tinyxml2/archive/4.0.1.tar.gz
tar xf 4.0.1.tar.gz
rm 4.0.1.tar.gz
cd tinyxml2-4.0.1/
mkdir -p build/
cd build/
rm -rf *
cmake .. -DBUILD_STATIC_LIBS=TRUE -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake
make 
cd ../../

wget http://zlib.net/zlib-1.2.8.tar.gz
tar xf zlib-1.2.8.tar.gz
rm zlib-1.2.8.tar.gz
cd zlib-1.2.8/
mkdir -p build/
cd build/
rm -rf *
cmake .. -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake
make 
cd ../../

git clone --depth=1 https://github.com/bulletphysics/bullet3.git
cd bullet3/
mkdir -p build/
cd build/
rm -rf *
cmake .. -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake -DBUILD_CPU_DEMOS=FALSE -DUSE_GLUT=OFF -DUSE_GRAPHICAL_BENCHMARK=OFF -DBUILD_BULLET2_DEMOS=OFF -DBUILD_OPENGL3_DEMOS=OFF -DBUILD_UNIT_TESTS=OFF
make 
cd ../../

git clone --depth=1 https://gitlab.com/CollectiveTyranny/pluto.git
cd pluto/
mkdir -p build
cd build/
rm -rf *
cmake .. -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake
make 
cd ../../

git clone --depth=1 https://github.com/cginternals/cpplocate.git
cd cpplocate/
mkdir -p build
cd build/
rm -rf *
cmake .. -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake
make 
cd ../../
'

cd ../lib-woe32/
:'
wget https://github.com/ZahlGraf/IrrlichtCMake/archive/v0.2.2.tar.gz
tar xf v0.2.2.tar.gz
rm v0.2.2.tar.gz

cd IrrlichtCMake-0.2.2/
#wget http://downloads.sourceforge.net/irrlicht/irrlicht-1.8.4.zip
#unzip irrlicht-1.8.4.zip
#rm -rf examples/ tools/
#mv irrlicht-1.8.4/* .
#rm -rf irrlicht-1.8.4/
#mkdir -p build/
cd build/
cmake .. -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake -DIRRLICHT_ENABLE_DIRECTX9=OFF -DIRRLICHT_BUILD_EXAMPLES=OFF -DIRRLICHT_INSTALL_MEDIA_FILES=OFF -DIRRLICHT_BUILD_TOOLS=OFF -DIRRLICHT_STATIC_LIBRARY=TRUE
make
cd ../
#mkdir -p build-shared/
cd build-shared/
cmake .. -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake -DIRRLICHT_ENABLE_DIRECTX9=OFF -DIRRLICHT_BUILD_EXAMPLES=OFF -DIRRLICHT_INSTALL_MEDIA_FILES=OFF -DIRRLICHT_BUILD_TOOLS=OFF
make
cd ../../

wget https://github.com/ZahlGraf/IrrIMGUI/archive/v0.3.1.tar.gz
tar xf v0.3.1.tar.gz
rm v0.3.1.tar.gz
cd IrrIMGUI-0.3.1/

cd dependency/
rm -rf IMGUI/
git clone --depth=1 https://github.com/ocornut/imgui.git
mv imgui/ IMGUI/
cd ../
mkdir -p build
cd build
rm -rf *
cmake .. -DCMAKE_TOOLCHAIN_FILE=../../../Toolchain-mingw32.cmake -DIRRIMGUI_INSTALL_MEDIA_FILES=OFF -DIRRIMGUI_BUILD_UNITTESTS=OFF -DIRRIMGUI_NATIVE_OPENGL=ON -DIRRIMGUI_BUILD_EXAMPLES=OFF -DIRRLICHT_BIN_DIR=../../IrrlichtCMake-0.2.2/build-shared/libIrrlicht.dll -DIRRLICHT_LIB_DIR=../../IrrlichtCMake-0.2.2/build/libIrrlicht.a -DIRRLICHT_INCLUDE_DIR=../../IrrlichtCMake-0.2.2/include/ -DIRRIMGUI_STATIC_LIBRARY=TRUE
make 
cd ../../../

mkdir build-woe32/
'

cd build-woe32/
cmake .. -DCMAKE_TOOLCHAIN_FILE=../Toolchain-mingw32.cmake
make 
