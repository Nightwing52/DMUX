#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <iostream>
#include <stdlib.h>
#include <BulletCollision/CollisionShapes/btCollisionShape.h>

#include "Arena.hpp"
#include "Simulation.hpp"
#include "client/JukeBox.hpp"
#include "sys/TriMesh.hpp"
#include "sys/Graphics.hpp"
#include "sys/Physics.hpp"
#include "DmuxCommon.hpp"

std::vector<irr::core::vector3df> Arena::spawnPoints;

void Arena::initXMLDoc(tinyxml2::XMLDocument &doc) {

    std::string xmlLocation = sceneDir + graphics.objStr + "/" + graphics.objStr + ".scene";

    try {
        tinyxml2::XMLError eResult = doc.LoadFile(xmlLocation.c_str());

        if(eResult not_eq tinyxml2::XML_SUCCESS) {
            throw std::string("Error: XML file does not exist or is corrupt (like Hillary Clinton)");
        }
    } catch(const std::string e) {
        doc.PrintError();
        std::cerr << e << std::endl;
        std::cerr <<
                  "DMUX is going to have to make an educated guess at where your tires will connect, where the vehicle's weapon gets mounted, etc. This error WILL NOT be solved by restarting the game."
                  << std::endl <<
                  "If you can replicate this error please email the DMUX developers at bkeys@gnu.org with the error message, your operating system, and version of DMUX you have installed"
                  << std::endl;
        return;
    }
}

void Arena::readSceneXML() {
    tinyxml2::XMLDocument doc;
    sceneDir = cpplocate::findModule("dmux").value("sceneDir");
    initXMLDoc(doc);

    tinyxml2::XMLElement *root = doc.FirstChildElement("scene");
    tinyxml2::XMLElement *nodes = root->FirstChildElement("nodes");

    std::string nodeName;
    for(tinyxml2::XMLElement *e = nodes->FirstChildElement("node"); e not_eq nullptr;
            e = e->NextSiblingElement("node")) {
        nodeName = e->Attribute("name");
        std::string mystr = nodeName.substr(0, nodeName.find(".", 0));

        if(nodeName == "MetaData") {
            for(tinyxml2::XMLElement *userData = e->FirstChildElement("user_data");
                    userData not_eq nullptr;
                    userData = userData->NextSiblingElement("user_data")) {
                metaData[userData->FirstAttribute()->Value()] = userData->Attribute("value");
            }
        }

        for(unsigned int i = 0; i < 3; ++i) {
            if(mystr == "Spawn" + std::to_string(i)) {
                irr::core::vector3df position;
                position.X = e->FirstChildElement("position")->FloatAttribute("x");
                position.Y = e->FirstChildElement("position")->FloatAttribute("y");
                position.Z = e->FirstChildElement("position")->FloatAttribute("z");
                position *= irr::f32(4.0f); // We have to multiply by 4 because we scale the arena by 4
                spawnPoints.push_back(position);
            }
        }

        if(mystr == "Light") {
            irr::core::vector3df position;
            position.X = e->FirstChildElement("position")->FloatAttribute("x");
            position.Y = e->FirstChildElement("position")->FloatAttribute("y");
            position.Z = e->FirstChildElement("position")->FloatAttribute("z");
            position *= irr::f32(4.0f); // We have to multiply by 4 because we scale the arena by 4

            for(tinyxml2::XMLElement *userData = e->FirstChildElement("user_data");
                    userData not_eq nullptr;
                    userData = userData->NextSiblingElement("user_data")) {
                irr::u32 r;
                irr::u32 g;
                irr::u32 b;
                r = userData->FloatAttribute("r");
                g = userData->FloatAttribute("g");
                b = userData->FloatAttribute("b");
                irr::video::SColor color(255, r, g, b);
                Simulation::device->getSceneManager()->addLightSceneNode(0, position, color);
            }
        }
    }
}

irr::core::vector3df Arena::getSpawnPoint() {
    return spawnPoints[rand() % spawnPoints.size()];
}

Arena::Arena(const std::string &arenaName) {

    graphics.objType = "tracks";
    graphics.objStr =  arenaName.c_str();

    readSceneXML();

    core::sys::addNode(graphics);

    physics.mesh = Simulation::device->getSceneManager()->getMesh(graphics.modelPath.c_str());
    physics.mass = btScalar(0.0f);
    physics.position = btVector3(0.0f, 0.0f, 0.0f);

    btBvhTriangleMeshShape *bvhShape = nullptr;
    physics.shape = core::sys::buildCollisionShape(physics.mesh, bvhShape);
    physics.shape->setLocalScaling(btVector3(4.0f, 4.0f, 4.0f));
    physics.shape->setMargin(btScalar(0.07f));
    core::sys::addToWorld(physics);
    // Store a pointer to the irrlicht node so we can update it later
    physics.rigidBody->setUserPointer(static_cast<void *>(graphics.node));
}
