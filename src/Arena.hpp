#pragma once

/**
 * @file   Arena.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @author Nicolas Ortega (deathsbreed@themusicinnoise.net)
 * @brief  Poorly written class that loads the map for the players
 */

#include <string>
#include <vector>
#include <map>
#include <tinyxml2.h>
#include <vector3d.h>
#include "comp/Graphics.hpp"
#include "comp/Physics.hpp"

/**
 * \brief  Poorly written class that loads the map for the players
 */
class Arena final {
public:

    /**
     * \brief Constructor for Arena class
     * \details initializes the arena according to the data
     * specific about the world, right now it just loads a
     * map file
     * \param wData The world data the arena is being placed in
     */
    Arena(const std::string &arenaName);
    static irr::core::vector3df getSpawnPoint();
private:
    std::string sceneDir;
    void initXMLDoc(tinyxml2::XMLDocument &doc);
    void readSceneXML();
    core::comp::Graphics graphics;
    core::comp::Physics physics;
    static std::vector<irr::core::vector3df> spawnPoints;

    std::map<std::string, std::string> metaData;
};
