#include <iostream>
#include <cassert>
#include <sstream>
#include <fstream>
#include "DmuxCommon.hpp"
#include <IrrlichtDevice.h>
#include <IFileSystem.h>
#include <irrString.h>

btVector3 irrToBtVector(const irr::core::vector3df &vec) {
    return btVector3(vec.X, vec.Y, vec.Z);
}

irr::core::vector3df btVectorToIrr(const btVector3 &vec) {
    return irr::core::vector3df(vec.getX(), vec.getY(), vec.getZ());
}

irr::core::vector3df stringToIrrVector(const std::string &v) {
#ifndef NDEBUG
    assert(not v.empty());
#endif
    std::istringstream iss {v};
    irr::f32 x, y, z;
    iss >> x >> y >> z;
    return irr::core::vector3df(x, y, z);
}

irr::core::vector3df getMeshSize(const irr::scene::IMesh *mesh) {
#ifndef NDEBUG
    assert(mesh not_eq nullptr);
#endif

    irr::core::vector3d<irr::f32> *edges = new irr::core::vector3d<irr::f32>[8];
    irr::core::aabbox3d<irr::f32> boundingbox = mesh->getBoundingBox();
    boundingbox.getEdges(edges);
    irr::f32 height = edges[1].Y - edges[0].Y; //OK
    irr::f32 width = edges[5].X - edges[1].X;
    irr::f32 depth = edges[2].Z - edges[0].Z;
    return irr::core::vector3df(height, width, depth);
}

bool fileExists(const std::string &name) {
    std::ifstream f(name);
    return f.good();
}

void processDirectories(const std::string &directory, const std::string &fileExtension, std::function<void(const std::string &, const std::string &)> processFunc) {

    irr::IrrlichtDevice *tmp = irr::createDevice(irr::video::EDT_NULL);
    irr::io::IFileSystem *fileSystem = tmp->getFileSystem();

    fileSystem->changeWorkingDirectoryTo(directory.c_str());
    irr::io::IFileList *fileList = fileSystem->createFileList();

    for(irr::u32 i = 0; i < fileList->getFileCount(); ++i) {
        if(fileList->isDirectory(i)) {
            std::string fileName;
            for(char letter : std::string(fileList->getFileName(i).c_str())) {
                if(not (letter == '.')) {
                    fileName.push_back(letter);
                }
            }

            std::string directoryName = fileList->getFullFileName(i).c_str();
            // The .scene file we are inspecting
            std::string fileToInspect = directoryName + "/" + fileName + fileExtension;


            if(fileExists(fileToInspect)) {
                std::cout << "|" << fileName << std::endl;
                std::cout << "\\" << fileToInspect << std::endl;
                //File name is also the objstr
                processFunc(fileName, fileToInspect);
            }
        }
    }

    tmp->drop();
}
