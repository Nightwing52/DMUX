#pragma once

// Please replace this with forward declares
#include <irrlicht.h>
#include <LinearMath/btVector3.h>
#include <string>
#include <vector>
#include <memory>

irr::core::vector3df stringToIrrVector(const std::string &v); //!< converts 3 floating values in a string seperated by white space into an Irrlicht vector
irr::core::vector3df getMeshSize(const irr::scene::IMesh *mesh); //!< get the 3D dimensions of a mesh
irr::core::vector3df btVectorToIrr(const btVector3 &vec);

// DEPRECATED please use Irrlicht's filesystem for checking for existing files
bool fileExists(const std::string &name); //!< quick function to see if file exists

btVector3 irrToBtVector(const irr::core::vector3df &vec); //!< conversion function

template<class T>
std::shared_ptr<const char *>toSharedPointer(const T &container) {

    std::shared_ptr<const char *>items(new const char *[container.size()]);
    for(size_t i = 0; i < container.size(); ++i) {
        items.get()[i] = container[i].data();
    }
    return items;
}


/*
Iterates through all subdirectories of specified directory,

Takes a function as a parameter, here is the documentation for the parameters
the first string is the directory being iterated through
the second string is name of the subdirectory within the first string
The function is supposed to take these two strings and process them with something.
You are going to want to capture your values within a lambda function when using this
function that way you have data to operate on and modify
*/
void processDirectories(const std::string &directory, const std::string &fileExtension, std::function<void(const std::string &, const std::string &)> processFunc);
