#include "Layout.hpp"
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>
#include <sstream>
#include "sys/XmlParser.hpp"

Layout::Layout() :
    isDefault(false) {

}

std::map<std::string, btScalar> Layout::generateStats() {
    std::map<std::string, btScalar> statsContainer;
    std::vector<std::string> statNames; // container so we can future proof when we modify available stats
    statNames.push_back("Acceleration");
    statNames.push_back("Handling");
    statNames.push_back("Weight");
    statNames.push_back("Armor");
    statNames.push_back("Max Speed");

    for(const auto &statName : statNames) {

        // It is needed to convert the strings to floats using string streams
        std::istringstream tireStringStream {tire[statName]};
        std::istringstream chassisStringStream {chassis[statName]};

        // holds the values of the actual stats, added together later
        btScalar tireAmount;
        btScalar chassisAmount;

        // Converting the strings to the appropriate floating value
        tireStringStream >> tireAmount;
        chassisStringStream >> chassisAmount;

        // We add the computed stat
        statsContainer[statName] = btScalar(tireAmount + chassisAmount);
    }

    return statsContainer;
}

std::map<std::string, std::string> Layout::getChassis() {
    return chassis;
}

void Layout::setChassis(const std::string &chass) {
    for(auto &chassisData : core::sys::getSceneMetaDataFromDir(cpplocate::findModule("dmux").value("chassisDir"))) {
        if(chassisData["objStr"] == chass) {
            chassis = chassisData;
        }
    }
    wheelPositions = core::sys::getWheelPositions()[chassis["objStr"]];
}

std::map<std::string, std::string> Layout::getTire() {
    return tire;
}

void Layout::setTire(const std::string &nTire) {
    for(auto &tireData : core::sys::getSceneMetaDataFromDir(cpplocate::findModule("dmux").value("tireDir"))) {
        if(tireData["objStr"] == nTire) {
            tire = tireData;
        }
    }
}

std::map<std::string, btVector3> Layout::getWheelPositions() {
    assert(wheelPositions.size() > 0);
    return wheelPositions;
}
