#pragma once

/**
 * @file   Layout.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Layout file containing the stats and
 * metadata for a given vehicle layout
 *
 * Copyright (c) 2016 Brigham Keys, Esq.
 */

#include <LinearMath/btScalar.h>
#include <LinearMath/btVector3.h>
#include <string>
#include <vector>
#include <map>

/**
 * \brief Class that controls the configuration for a given vehicle
 * \details Vehicle settings, this class is passed around anytime there is 
 * a definition of a vehicle needed. Instances of layouts are created by
 * the player in the Garage
 */
class Layout final {

public:

    Layout();
    std::string name; //!< The name specified by the user for the layout

    /**
     * \brief Builds a container for the stats of the layout
     * \details Takes the data in the tire and chassis members
     * and combines the layout stats and returns an STL container
     * of the computed stats
     */
    std::map<std::string, btScalar> generateStats();

    std::map<std::string, std::string> getChassis(); //!< returns map of all data about the tire
    void setChassis(const std::string &chass); //!< Sets the chassis using an objStr

    std::map<std::string, std::string> getTire(); //!< returns map of all data about the tire
    void setTire(const std::string &nTire); //!< Sets the tire using an objStr

    /*
     * \brief Gets the positions of the wheels for the layout
     */
    std::map<std::string, btVector3> getWheelPositions();

    /**
     * \brief Simple function required by cereal to serialize the layout
     */
    template <class Archive>
    void serialize(Archive &ar) {
        ar(name, tire, chassis, isDefault);
    }
    bool isDefault; //!< Whether or not the layout is the default layout for the user

private:

    /**
     * \brief The tire selected by the user for the layout
     * \details Contains all the attributes about the tire including it's stats and
     * other metadata about it. This information can be found in the "MetaData" node
     * of the given .scene. These .scene files can be found in the subdirectories of
     * the assets/tire/ directory.
     */
    std::map<std::string, std::string> tire;

    /**
     * \brief The chassis selected by the user for the layout
     * \details Contains all the attributes about the chassis including it's stats and
     * other metadata about it. This information can be found in the "MetaData" node
     * of the given .scene. These .scene files can be found in the subdirectories of
     * the assets/chassis/ directory.
     */
    std::map<std::string, std::string> chassis;

    std::map<std::string, btVector3> wheelPositions; //!< Positions for each tire
};
