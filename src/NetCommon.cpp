#include "NetCommon.hpp"
#include <RakNetStatistics.h>
#include <RakNetTypes.h>

GameServerList NetCommon::serverList;

NetCommon::NetCommon() :
    //    interface(RakNet::RakPeerInterface::GetInstance()),
    password("Brigham Keys") {
}

NetCommon::~NetCommon() {
    // We're done with the network
    for(auto &instance : interface) {
        instance.second->Shutdown(300);
        RakNet::RakPeerInterface::DestroyInstance(instance.second);
    }
}

void NetCommon::printServerList() {
    LOG(INFO) << "Available Servers";
    for(auto entry : serverList.data) {
        LOG(INFO) << entry.second.serverName;
    }
}
