#include "NetworkQueue.hpp"
#include <MessageIdentifiers.h>

namespace core {
namespace net {

    void NetworkQueue::connect(const RakNet::SystemAddress &nAddress) {

        interface["server"] = RakNet::RakPeerInterface::GetInstance();
        interface["server"]->SetOccasionalPing(true);

        RakNet::SocketDescriptor desc;

        bool b = interface["server"]->Startup(2, &desc, 1) == RakNet::RAKNET_STARTED;
        if(!b) {
            LOG(ERROR) << "Failed to start IPV4 ports. Time to kill yourself.";
        } else {
            LOG(INFO) << "The bound to game server just fine.";
        }

        RakNet::ConnectionAttemptResult car = interface["server"]->Connect(nAddress.ToString(false), nAddress.GetPort(), "Brigham Keys", std::string("Brigham Keys").length());
        assert(car == RakNet::CONNECTION_ATTEMPT_STARTED);
    }

void NetworkQueue::operator()() {
    for(packet = interface["server"]->Receive();
            packet;
            interface["server"]->DeallocatePacket(packet), packet = interface["server"]->Receive()) {
        switch(packet->data[0]) {
            case ID_CONNECTION_REQUEST_ACCEPTED:
                LOG(INFO) << "Connection to server at " << packet->systemAddress.ToString(false) << " succeeded.";
            case ID_DISCONNECTION_NOTIFICATION:
                LOG(INFO) << "Disconnected from server at " << packet->systemAddress.ToString(false);
                break;
        }
    }
    return;
}
}
}
