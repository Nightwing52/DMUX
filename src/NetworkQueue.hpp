#include <queue>
#include "NetCommon.hpp"

namespace RakNet {
    class SystemAddress;
}

namespace core {
namespace net {
    //template<class NetworkType>
class NetworkQueue final : public NetCommon {
public:
    void connect(const RakNet::SystemAddress &nAddress);
    void operator()();
};
}
}
