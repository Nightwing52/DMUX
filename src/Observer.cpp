#include "Observer.hpp"
#include "Simulation.hpp"
#include <easylogging++.h>

#include <UUID4.h>

namespace core {

Observer::Observer() :
    ID(plt::GenUUID()) {
    LOG(INFO) << "Observer created with ID: " + ID;
}
}
