#pragma once
// NOTE: we use the double negative `!!' so the macro forces bool type.
#define likely(x) __builtin_expect(!!(x), true)
#define unlikely(x) __builtin_expect(!!(x), false)
