#include "PacketTypes.hpp"
#include <cstring>
#ifndef NDEBUG
#include <cassert>
#endif //NDEBUG

char *createChatMessagePacket(const char *msg) {
    // Create struct
    ChatMessagePacket packetStruct;
    std::memcpy(packetStruct.msg, msg, sizeof(packetStruct.msg));
    // TODO: Serialize the struct
    // Create the char array packet
    char *packet = nullptr;
    std::memcpy(packet, &packetStruct, sizeof(ChatMessagePacket));
#ifndef NDEBUG
    assert(packet not_eq nullptr);
#endif //NDEBUG
    return packet;
}

char *createActionKeysPacket(bool actionKeysDown[NUM_ACTION_KEYS]) {
    ActionKeysPacket packetStruct;
    for(int i = 0; i < NUM_ACTION_KEYS; ++i) {
        packetStruct.actionKeys[i] = actionKeysDown[i];
    }
    char *packet = nullptr;
    std::memcpy(packet, &packetStruct, sizeof(ActionKeysPacket));
#ifndef NDEBUG
    assert(packet not_eq nullptr);
#endif //NDEBUG
    return packet;
}

char *createGunRotationPacket(irr::f32 x, irr::f32 y, irr::f32 z) {
    GunRotationPacket packetStruct;
    packetStruct.x = x;
    packetStruct.y = y;
    packetStruct.z = z;
    char *packet = nullptr;
    std::memcpy(packet, &packetStruct, sizeof(GunRotationPacket));
#ifndef NDEBUG
    assert(packet not_eq nullptr);
#endif //NDEBUG
    return packet;
}
