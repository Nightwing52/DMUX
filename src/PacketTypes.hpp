#pragma once
#include <irrTypes.h>
/**
 * \brief Used in identifying packets sent & received.
 * \details Each of these enumerations represent a different packet type
 * which is used by the network handlers (on both the client and the server)
 * to distinguish packet types from one another.
 */
enum {
    ACTIONS_KEYS_PRESSED    = '0', //!< Packet will contain an array stating if action keys are pressed or not
    GUN_ROTATION            = '1', //!< Packet will contain the rotation of the mounted gun
    ENTITY_LIST             = '2', //!< Packet will contain a list of entities in game
    SERV_LIST_REQUEST       = '3', //!< Packet for requesting the server list from the master server
    CHAT_MESSAGE            = '4', //!< Packet for chat messages
    CHAT_COMMAND            = '5', //!< Packet for chat commands
    SERV_LIST               = '6' //!< Packet contains list of servers
};

/**
 * \brief Used to attach user specified key press to an action.
 * \details In order to send key presses to the server in such a way that everyone
 * can have their own custom key presses these action keys are sent in its stead.
 */
enum {
    ACCEL_FORWARD           = 0x00, //!< Forward key is pressed
    ACCEL_REVERSE           = 0x01, //!< Backward key is pressed
    STEER_LEFT              = 0x02, //!< Left key is pressed
    STEER_RIGHT             = 0x03, //!< Right key is pressed
    HEADLIGHT_TOGGLE        = 0x04, //!< Light key is pressed
    ENABLE_CHAT             = 0x05, //!< A client has enabled in game chat
    NUM_ACTION_KEYS         = 0x06 //!< The total number of action keys (used for array sizes and such)
};

#pragma pack(push, 1)
/**
 * \brief Packet for chat message
 * \details This packet contains a message to be delivered to a server along
 * with a few important parameters.
 */
struct ChatMessagePacket {
    unsigned char typeId = CHAT_MESSAGE;
    // DATA:
    char msg[258]; // The message to be sent
};
#pragma pack(pop)

/**
 * \brief Used to create a packet for chat messages.
 * @param[in] msg The chat message to send.
 * \returns Returns the struct to be sent over the network.
 */
char *createChatMessagePacket(const char *msg);

#pragma pack(push, 1)
/**
 * \brief Packet for action keys.
 * \details This packet contains the action keys that are and are not pressed
 * in order to send such information to the server.
 */
struct ActionKeysPacket {
    //unsigned char useTimeStamp;
    //RakNet::Time timeStamp;
    unsigned char typeId = ACTIONS_KEYS_PRESSED;
    // DATA:
    bool actionKeys[NUM_ACTION_KEYS];
};
#pragma pack(pop)

/**
 * \brief Used to create a packet for action keys.
 * \details Function used to create a char pointer packet that can be sent to
 * the server without any further trouble.
 * @param[in] actionKeysDown The action keys that will be put in the packet
 * \return Returns a struct to be sent over the network.
 */
char *createActionKeysPacket(bool actionKeysDown[NUM_ACTION_KEYS]);

#pragma pack(push, 1)
/**
 * \brief Packet for gun rotation.
 * \details This packet contains the yaw, pitch, and rotation of the gun in
 * order to send such information to the server.
 */
struct GunRotationPacket {
    //unsigned char useTimeStamp;
    //RakNet::Time timeStamp;
    unsigned char typeId = GUN_ROTATION;
    // DATA:
    irr::f32 x, y, z;
};
#pragma pack(pop)

/**
 * \brief Used to create a packet for the gun's rotation.
 * \details Function used to create a char pointer packet that can be sent to
 * the server without any further trouble.
 * @param[in] x The yaw of the rotation.
 * @param[in] y The pitch of the rotation.
 * @param[in] z The roll of the rotation.
 * \return Returns a struct to be sent over the network.
 */
char *createGunRotationPacket(irr::f32 x, irr::f32 y, irr::f32 z);
