#include <IrrlichtDevice.h>
#include <BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>
#include "Simulation.hpp"
#include "sys/World.hpp"
#ifndef NDEBUG
#include <cassert>
#endif //NDEBUG
#include "DebugDraw.hpp"

irr::IrrlichtDevice *Simulation::device;
core::comp::World Simulation::mir;

Simulation::Simulation(irr::IrrlichtDevice *parentDevice) {
    device = parentDevice;
#ifndef NDEBUG
    assert(device not_eq nullptr);
    //assert the contents of Mir, not Mir itself
#endif //NDEBUG
    mir.isDebugDraw = false;
    debugMat.Lighting = true;
}

void Simulation::step(irr::u32 &timeStamp, irr::u32 &deltaTime) {

    core::sys::updateScreen(mir);
    deltaTime = (device->getTimer()->getTime() - timeStamp);
    timeStamp = device->getTimer()->getTime();

#ifndef NDEBUG
    assert(mir.world not_eq nullptr);
#endif //NDEBUG

    notify_observers(&core::Observer::notify);
    mir.world->stepSimulation(deltaTime * 0.001f, 60);

    if(mir.isDebugDraw) {
        device->getVideoDriver()->setMaterial(debugMat);
        device->getVideoDriver()->setTransform(irr::video::ETS_WORLD,
                                               irr::core::IdentityMatrix);
        mir.world->debugDrawWorld();
    }
}

Simulation::~Simulation() {
}
