#pragma once

/**
 * @file   MainMenuInput.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 */

#include <vector>
#include <SMaterial.h>
#include "comp/World.hpp"
#include "Observer.hpp"
#include "obs/observable.h"
#include "Vehicle.hpp"

namespace irr {
    class IrrlichtDevice;
namespace video {
    class SMaterial;
}
}
/**
 * \brief Handles the game simulation, ran on both client and server
 * \details Contains the vehicles and everything in the physical world of the game
 * the information for this class is synced between th server and client
 */
class Simulation final : public obs::observable<core::Observer> {
public:
    /**
     * \param parentDevice specifies the root device
     */
    Simulation(irr::IrrlichtDevice *parentDevice);

    /**
     * \brief ticks the simulation
     * \param timeStamp The current time
     * \param deltaTime determines time between now and last frame
     */
    void step(irr::u32 &timeStamp, irr::u32 &deltaTime);
    ~Simulation();

    static irr::IrrlichtDevice *device; //!< Irrlicht device used for loading models
    static core::comp::World mir; //!< DMUX world, built on top of Bullet physics world

private:
    irr::video::SMaterial debugMat; //!< used for debug draw
};
