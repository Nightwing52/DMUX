#include <BulletDynamics/Vehicle/btRaycastVehicle.h>
#include <BulletCollision/CollisionShapes/btConvexHullShape.h>
#include <BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>

#include "DmuxCommon.hpp"
#include <tinyxml2.h>
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <easylogging++.h>
#ifndef NDEBUG
#include <cassert>
#include <iostream>
#endif

#include "client/Game.hpp"
#include "Vehicle.hpp"
#include "Simulation.hpp"
#include "sys/TriMesh.hpp"
#include "sys/Graphics.hpp"
#include "sys/Physics.hpp"
#include "gui/Garage.hpp"

std::string Vehicle::chassisDir = cpplocate::findModule("dmux").value("chassisDir");
std::string Vehicle::tireDir = cpplocate::findModule("dmux").value("tireDir");

Vehicle::Vehicle(const irr::core::vector3df &pos) :
    Observer(),
    wheelDirectionCS0(0, -1, 0),
    wheelAxleCS(-1, 0, 0),
    suspensionRestLength(0.5),
    wheelWidth(0.8),
    wheelRadius(0.5f),
    connectionHeight(1.0),
    steer(0),
    layout() {

    layout = menu::Garage::getDefaultLayout();

    graphics.objType = "chassis";
    graphics.objStr  = layout.getChassis()["objStr"];

    core::sys::addNode(graphics);

    //    parseKartXML();
    std::string wheelName = tireDir + layout.getTire()["objStr"] + "/" + layout.getTire()["objStr"] + ".b3d";

    rearRightWheel = Simulation::device->getSceneManager()->addMeshSceneNode(
                         Simulation::device->getSceneManager()->getMesh(wheelName.c_str()));

    wheelRadius = (getMeshSize(rearRightWheel->getMesh()).Y);
    wheelWidth = (getMeshSize(rearRightWheel->getMesh()).X);

    rearRightWheel->setParent(graphics.node);
    rearRightWheel->setPosition(btVectorToIrr(layout.getWheelPositions()["br-wheel"]));

    rearLeftWheel = Simulation::device->getSceneManager()->addMeshSceneNode(
                        Simulation::device->getSceneManager()->getMesh(wheelName.c_str()));
    rearLeftWheel->setParent(graphics.node);
    rearLeftWheel->setPosition(btVectorToIrr(layout.getWheelPositions()["bl-wheel"]));

    frontLeftWheel = Simulation::device->getSceneManager()->addMeshSceneNode(
                         Simulation::device->getSceneManager()->getMesh(wheelName.c_str()));
    frontLeftWheel->setParent(graphics.node);
    frontLeftWheel->setPosition(btVectorToIrr(layout.getWheelPositions()["fl-wheel"]));

    frontRightWheel = Simulation::device->getSceneManager()->addMeshSceneNode(
                          Simulation::device->getSceneManager()->getMesh(wheelName.c_str()));
    frontRightWheel->setParent(graphics.node);
    frontRightWheel->setPosition(btVectorToIrr(layout.getWheelPositions()["fr-wheel"]));

    //Never deactivate the vehicle
    physics.position = btVector3(pos.X, pos.Y, pos.Z);
    physics.mass = layout.generateStats()["Weight"];
    physics.mesh = Simulation::device->getSceneManager()->getMesh(graphics.modelPath.c_str());
    physics.shape = core::sys::buildConvexHullShape(physics.mesh);
    core::sys::addToWorld(physics);
    physics.rigidBody->setUserPointer(static_cast<void *>(graphics.node));

    Game::sim.add_observer(this);
    physics.rigidBody->setActivationState(DISABLE_DEACTIVATION);
    physics.rigidBody->setDamping(0.2, 0.2);

#ifndef NDEBUG
    assert(physics.shape not_eq nullptr);
    assert(physics.rigidBody not_eq nullptr);
#endif
    vehicleRayCaster = new btDefaultVehicleRaycaster(Simulation::mir.world);

    //Creates a new instance of the raycast vehicle
    btVehicle = new btRaycastVehicle(tuning, physics.rigidBody, vehicleRayCaster);

#ifndef NDEBUG
    assert(btVehicle not_eq nullptr);
    assert(Simulation::mir.world not_eq nullptr);
#endif

    //Adds the vehicle to the world
    Simulation::mir.world->addVehicle(btVehicle);

    //Adds the wheels to the vehicle
    addWheels(btVehicle, tuning);

    LOG(INFO) << "Vehicle with ID " << ID << " successfully spawned";
}

void Vehicle::setEngineForce(btScalar &f) {

    f *= layout.generateStats()["Max Speed"];

    if(f not_eq 0) {
        if(steer < btScalar(0.005f) and steer > btScalar(-0.005f)) {
            steer = btScalar(0);
        } else if(steer > 0) {
            steer -= btScalar(0.005f);
        } else {
            steer += btScalar(0.005f);
        }
    }
    engineForce = f;

    btVehicle->setSteeringValue(btScalar(steer), 0);
    btVehicle->setSteeringValue(btScalar(steer), 1);

    btVehicle->applyEngineForce(engineForce, 2);
    btVehicle->applyEngineForce(engineForce, 3);
    return;
}

/*this is a really bad name and should be changed*/
void Vehicle::updateTires(const btScalar s) {

    steer += s;
    btScalar range = btScalar(.15);
    if(steer not_eq 0) {
        if(steer > range) {
            steer = range;
        } else if(steer < -range) {
            steer = -range;
        }
    }
    btVehicle->setSteeringValue(btScalar(steer), 0);
    btVehicle->setSteeringValue(btScalar(steer), 1);
}

void Vehicle::notify() {
    btScalar rate = btScalar(1000.0f);
    rearRightWheel->setRotation(irr::core::vector3df(btVehicle->getWheelInfo(
                                    0).m_rotation * rate,
                                irr::f32(0),
                                irr::f32(0)));

    frontLeftWheel->setRotation(irr::core::vector3df(-btVehicle->getWheelInfo(
                                    2).m_rotation * rate,
                                irr::f32((steer * 180) - 180),
                                irr::f32(0)));

    frontRightWheel->setRotation(irr::core::vector3df(btVehicle->getWheelInfo(
                                     3).m_rotation * rate,
                                 irr::f32(steer * 180),
                                 irr::f32(0)));

    rearLeftWheel->setRotation(irr::core::vector3df(-btVehicle->getWheelInfo(
                                   1).m_rotation * rate,
                               irr::f32(180),
                               irr::f32(0)));
}

void Vehicle::addWheels(
    btRaycastVehicle *vehicle,
    btRaycastVehicle::btVehicleTuning tuning) {
    //The direction of the raycast, the btRaycastVehicle uses raycasts instead of simiulating the wheels with rigid bodies
    btVector3 wheelDirectionCS0(0, -1, 0);

    //The axis which the wheel rotates arround
    btVector3 wheelAxleCS(-1, 0, 0);

    btScalar suspensionRestLength(0.2);

    //Adds the front wheels
    vehicle->addWheel(layout.getWheelPositions()["fr-wheel"], wheelDirectionCS0, wheelAxleCS, suspensionRestLength, wheelRadius, tuning, true);
    vehicle->addWheel(layout.getWheelPositions()["fl-wheel"], wheelDirectionCS0, wheelAxleCS, suspensionRestLength, wheelRadius, tuning, true);

    //Adds the rear wheels
    vehicle->addWheel(layout.getWheelPositions()["br-wheel"], wheelDirectionCS0, wheelAxleCS, suspensionRestLength, wheelRadius, tuning, false);
    vehicle->addWheel(layout.getWheelPositions()["bl-wheel"], wheelDirectionCS0, wheelAxleCS, suspensionRestLength, wheelRadius, tuning, false);

    //Configures each wheel of our vehicle, setting its friction, damping compression, etc.
    //For more details on what each parameter does, refer to the docs
    for(int i = 0; i < vehicle->getNumWheels(); i++) {
        btWheelInfo &wheel = vehicle->getWheelInfo(i);
        wheel.m_suspensionStiffness = 50;
        wheel.m_wheelsDampingCompression = (btScalar(0.3) * 2) * btSqrt(wheel.m_suspensionStiffness);//btScalar(0.8);
        wheel.m_wheelsDampingCompression = btScalar(2.0f);
        wheel.m_wheelsDampingRelaxation = btScalar(3.0f);
        wheel.m_wheelsDampingRelaxation = (btScalar(0.5) * 2) * btSqrt(wheel.m_suspensionStiffness);//1;
        //Larger friction slips will result in better handling
        wheel.m_frictionSlip = btScalar(200.0f * layout.generateStats()["Handling"]);
        wheel.m_rollInfluence = 1;
    }
}
