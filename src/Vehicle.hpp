#pragma once

#include <string>
#include <vector3d.h>
#include "comp/Graphics.hpp"
#include "comp/Physics.hpp"
#include "Observer.hpp"
#include "Layout.hpp"
#include <BulletDynamics/Vehicle/btRaycastVehicle.h>

/**
 * \brief Vehicle in the gameplay, involves a chassis, wheel set and weapon
 * \details Contains the details for vehicle physics, parses the kart.xml
 * to figure out where the wheel set is located on the vehicle and where
 * the weapon is mounted
 */

namespace irr {
namespace scene {
    class IMeshSceneNode;
}
}

class Vehicle final : core::Observer {

public:
    /**
     * \brief Constructor for Vehicle
     * \details Initializes
     * - Places the Vehicle inside of the specified world
     * - Specifies the chassis to be loaded, eventually it will have the whole vehicle layout in an XML file
     * - Position in the world
     * \param pos Where the vehicle is going to be spawned
     */
    Vehicle(const irr::core::vector3df &pos = irr::core::vector3df(btScalar(0.0f)));

    /**
     * \brief Updates the Vehicle
     * \details Updates:
     * - Updates the rotation of the tires
     * - Executes event handling
     */
    void notify() override;

    /**
     * \brief Set the engine force of the vehicle, basically acceleration
     * \param f acceleration rate of vehicle THIS IS A TEMPORARY PUBLIC
     *  INTERFACE FOR TESTING AND IS NOT GUARENTEED TO BE HONORED
     */
    void setEngineForce(btScalar &f);

    /**
     * \brief Really bad name for this function, but updates the steering value
     * \param s Steering value to be added to vehicle THIS IS A TEMPORARY PUBLIC
     *  INTERFACE FOR TESTING AND IS NOT GUARENTEED TO BE HONORED
     */
    void updateTires(const btScalar s);
    core::comp::Physics physics;

private:

    core::comp::Graphics graphics;
    btRaycastVehicle *btVehicle; //!< Bullet vehicle
    btScalar engineForce; //!< The rate of acceleration the engine is driving
    irr::core::vector3df wheelDirectionCS0; //!< Which direction the wheel is facing
    irr::core::vector3df wheelAxleCS; //!< The axis which the wheel rotates arround
    btScalar suspensionRestLength; //!< Gotta figure that one out
    btScalar wheelWidth; //!< How wide the wheels are
    btScalar wheelRadius; //!< How large the wheels are
    btScalar connectionHeight; //!< The height where the wheels are connected to the chassis
    btScalar steer; //!< Current direction the vehicle is steering

    btRaycastVehicle::btVehicleTuning tuning; //!< vehicle tuning
    irr::scene::IMeshSceneNode *rearRightWheel;  //!< Irrlicht node for rear right wheel (Needs to be an entity)
    irr::scene::IMeshSceneNode *rearLeftWheel;   //!< Irrlicht node for rear left wheel (Needs to be an entity)
    irr::scene::IMeshSceneNode *frontLeftWheel;  //!< Irrlicht node for front left wheel(Needs to be an entity)
    irr::scene::IMeshSceneNode *frontRightWheel; //!< Irrlicht node for front right wheel (Needs to be an entity)
    irr::core::vector3df getXMLWheelPos(const char *a);  //!< Gets a wheel position from kart.xml, used in the initializer list
    irr::core::vector3df weaponMountPoint;  //!< Point on vehicle where the weapon is mounted
    btWheelInfoConstructionInfo wheel; //!< Information about the vehicles wheels
    void addWheels(btRaycastVehicle *vehicle,
                   btRaycastVehicle::btVehicleTuning tuning); //!< adds the wheels to the btVehicle
    btRaycastVehicle::btVehicleTuning m_tuning; //!< Vehicle tuning
    btVehicleRaycaster *vehicleRayCaster; //!< bullet data type for handling vehicle physics
    static std::string chassisDir; //!< asset path for the chassis
    static std::string tireDir; //!< asset path for the tires
    Layout layout;
};
