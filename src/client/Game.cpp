#include <thread>
#include <cassert>
#include <string>
#include <fstream>
#include <tinyxml2.h>
#include <IrrlichtDevice.h>
#include <IrrIMGUI/SIMGUISettings.h>
#include <easylogging++.h>
#include <MessageIdentifiers.h>
#include <BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>

#include "sys/World.hpp"
#include "gui/Menu.hpp"
#include "Game.hpp"
#include "comp/World.hpp"
#include "DebugDraw.hpp"
#include "Optimizers.hpp"

INITIALIZE_EASYLOGGINGPP

Settings Game::playerSettings;
irr::IrrlichtDevice *Game::device = createDeviceEx(playerSettings.getIrrlichtParams());
Simulation Game::sim(device);
DebugDraw *debugDraw; //!< Debug Drawer for world
menu::Menu *Game::menu;

//[[deprecated("Replaced by bar, which has an improved interface")]]
//http://stackoverflow.com/questions/295120/c-mark-as-deprecated?noredirect=1&lq=1
Game::Game() :
    NetCommon(),
    drawWireFrame(false),
    jBox(),
    serverPort("123411"), // This will have to depend on what the master server of the custom connect gives, since different servers may use different ports
    masterServerPort("16722"),
    ip("192.168.1.123"),
    socketDescriptor(std::stoi(serverPort), 0) {

    socketDescriptor.socketFamily = AF_INET;
    interface["master"] = RakNet::RakPeerInterface::GetInstance();

    // Connecting the server is very simple.  0 means we don't care about
    // a connectionValidationInteger, and false for low priority threads
    interface["master"]->Startup(8, &socketDescriptor, 1);

    interface["master"]->Connect(ip.c_str(), std::stoi(masterServerPort), password.c_str(), password.length());
    RakAssert(RakNet::CONNECTION_ATTEMPT_STARTED);
    LOG(INFO) << "Network is starting! Resources have been initialized";

    device->setWindowCaption(L"DMUX");
    device->getCursorControl()->setVisible(false);
#ifndef NDEBUG
    assert(device not_eq nullptr);
#endif

}

void Game::updateServerList() {

    for(packet = interface["master"]->Receive();
            packet;
            interface["master"]->DeallocatePacket(packet), packet = interface["master"]->Receive()) {
        switch(packet->data[0]) {
            case ID_CONNECTION_REQUEST_ACCEPTED:
                LOG(INFO) << "Connection to master server successful";
                sendPacket(interface["master"], ip.c_str(), 0, 140, 15, true); // 0 for an empty packet requesting server list
                break;
        }
        // We got a packet, get the identifier with our handy function
        if(packet->data[0] == 140) {
            if(packet->data[1] == 16) {
                LOG(INFO) << "Requesting new server list";
                NetCommon::serverList = deserializePacket<GameServerList>(packet);
            }
        }
    }
}

int Game::operator()() {
    core::sys::createWorld(sim.mir);
    debugDraw = new DebugDraw(Simulation::device);
    debugDraw->setDebugMode(
        btIDebugDraw::DBG_DrawWireframe |
        btIDebugDraw::DBG_DrawAabb |
        btIDebugDraw::DBG_DrawContactPoints |
        //btIDebugDraw::DBG_DrawText |
        //btIDebugDraw::DBG_DrawConstraintLimits |
        btIDebugDraw::DBG_DrawConstraints //|
    );

#ifndef NDEBUG
    assert(debugDraw not_eq nullptr);
#endif
    sim.mir.world->setDebugDrawer(debugDraw);

    device->getVideoDriver()->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, true);

    // Set our delta time and time stamp
    irr::u32 timeStamp = device->getTimer()->getTime();
    irr::u32 deltaTime = 0;
    std::thread jukeBoxThread(JukeBox::jukeBoxLoop);
    std::string selectedArena;

#ifndef NDEBUG
    //running our unit tests before we go into the game
    assert(sim.mir.collisionConfiguration not_eq nullptr);
    assert(sim.mir.dispatcher not_eq nullptr);
    assert(sim.mir.broadphase not_eq nullptr);
    assert(sim.mir.solver not_eq nullptr);
    assert(sim.mir.world not_eq nullptr);
    assert(device not_eq nullptr);
#endif

    menu = new menu::Menu();

    //Main menu loop
    {
        while(likely(menu->run() and device->run())) {
            device->getVideoDriver()->beginScene(true, true, irr::video::SColor(0, 0, 101, 140));
            selectedArena = menu->showMainMenu();
            device->getVideoDriver()->endScene();

            updateServerList();
        }
    }

    if(likely(device->run())) {
        //Game loop
        network.connect(menu->getCurrentServer());
        device->getCursorControl()->setVisible(false);
        {
            if(selectedArena.empty()) {
                selectedArena = "factory_BSW"; // set a default map
            }
            Arena a(selectedArena);
            Player b;
            //server.connect();
            //	JukeBox::setPlayList("olivermath"); //needs to be moved into Arena
            while(likely(device->run())) {
                device->getVideoDriver()->beginScene(true, true, irr::video::SColor(0, 0, 101, 240));
                sim.step(timeStamp, deltaTime);
                device->getSceneManager()->drawAll();
                b.show();
                device->getVideoDriver()->endScene();
                getServerPackets();
                network();
            }
        }
    }
    JukeBox::closed = true;
    jukeBoxThread.join();
    return 0;
}

void Game::getServerPackets() {
    for(packet = interface["master"]->Receive();
            packet;
            interface["master"]->DeallocatePacket(packet), packet = interface["master"]->Receive()) {
        switch(packet->data[0]) {
            case ID_CONNECTION_REQUEST_ACCEPTED:
                LOG(INFO) << "Connection to game server successful!";
                sendPacket(interface["master"], ip.c_str(), 0, 140, 15, true); // 0 for an empty packet requesting server list
                break;
        }
    }
}

void Game::connect() {

    RakNet::SystemAddress serverAddress = menu->getCurrentServer();

    RakNet::RakPeerInterface *interface;

    interface = RakNet::RakPeerInterface::GetInstance();
    interface->SetOccasionalPing(true);

    RakNet::SocketDescriptor desc;

    bool b = interface->Startup(2, &desc, 1) == RakNet::RAKNET_STARTED;
    if(!b) {
        LOG(ERROR) << "Failed to start IPV4 ports. Time to kill yourself.";
    } else {
        LOG(INFO) << "The bound to game server just fine.";
    }

    RakNet::ConnectionAttemptResult car = interface->Connect(serverAddress.ToString(false), serverAddress.GetPort(), "Brigham Keys", std::string("Brigham Keys").length());
    //    RakNet::ConnectionAttemptResult car = interface->Connect("192.168.1.124", 12347, "Brigham Keys", std::string("Brigham Keys").length());
    assert(car == RakNet::CONNECTION_ATTEMPT_STARTED);

}

Game::~Game() {
#ifndef NDEBUG
    assert(debugDraw not_eq nullptr);
    assert(device not_eq nullptr);
#endif
    delete menu;
    delete debugDraw;
    device->drop();
}
