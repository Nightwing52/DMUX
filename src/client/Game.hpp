#pragma once

/**
 * @file   Game.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @author Nicolas Ortega (deathsbreed@themusicinnoise.net)
 * @brief  Game class that is the parent of all other elements of DMUX
 */

#include <memory>
#include <map>

#include "DebugDraw.hpp"
#include "Simulation.hpp"
#include "JukeBox.hpp"
#include "Player.hpp"
#include "gui/Settings.hpp"
#include "NetCommon.hpp"
#include "NetworkQueue.hpp"

namespace irr {
    class IrrlichtDevice;
}

namespace menu {
class Menu;
}

/**
 * \brief Your typical game class
 * \details Container class for all the objects in DMUX
 */
class Game final : public NetCommon {

public:

    /**
     * \brief Default constructor for DMUX
     * \details Initializes:
     * - debug drawing properties
     * - jukebox for sound
     * - networking
     * - loads the user's settings file
     */
    Game();

    /**
     * \brief game loop for DMUX
     * \details Does some initialization of it's own, puts
     * elements into threads, and then runs the game loop
     */
    int operator()();

    ~Game();

    /**
     * \brief Irrlicht device
     * \details Please look this up on the Irrlict docs
     */
    static irr::IrrlichtDevice *device;
    static Simulation sim;

    static Settings playerSettings;
    static menu::Menu *menu;

private:
    
    void connect();
    void updateServerList();
    void getServerPackets();
    static std::string settingsDir;
    const bool drawWireFrame; //!< Whether or not to draw wireframes in debug draw mode
    const JukeBox jBox; //!< Sound Server for the game

    void recievePacketHandle();
    std::string serverPort;
    std::string masterServerPort;
    std::string ip;
    RakNet::SocketDescriptor socketDescriptor;
    core::net::NetworkQueue network;
};
