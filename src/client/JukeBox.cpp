#include "JukeBox.hpp"

#ifndef NDEBUG
#include <cassert>
#include <iostream>
#endif

#include <cmath>
#include <tinyxml2.h>
#include <random>
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <easylogging++.h>
#include "Game.hpp"

cAudio::IAudioManager *JukeBox::audioMgr;
std::vector<std::thread> JukeBox::sfx;
bool JukeBox::closed;
std::vector<Record> JukeBox::records;

std::string JukeBox::logDir = cpplocate::findModule("dmux").value("logDir");
std::string JukeBox::musicDir = cpplocate::findModule("dmux").value("musicDir");
std::string JukeBox::playlistDir = cpplocate::findModule("dmux").value("playlistDir");
std::string JukeBox::sfxDir = cpplocate::findModule("dmux").value("sfxDir");

// Main Menu is first thing that shows up
std::string JukeBox::loadedPlaylist = "MainMenu";
bool JukeBox::changePlaylist = false;
irr::f32 JukeBox::musicVolume;
irr::f32 JukeBox::masterVolume;
irr::f32 JukeBox::soundfxVolume;

std::string fetchRelPath(std::string path);
void replaceString(std::string &subject, const std::string &search,
                   const std::string &replace);

inline std::string fetchRelPath(std::string path) {
    path.erase(0, path.rfind("/") + 1);
    replaceString(path, "%20", " ");
    return path;
}

inline void replaceString(std::string &subject, const std::string &search,
                          const std::string &replace) {
    size_t pos = 0;
    while((pos = subject.find(search, pos)) not_eq std::string::npos) {
        subject.replace(pos, search.length(), replace);
        pos += replace.length();
    }
}

void JukeBox::readPlayListXML() {

    tinyxml2::XMLDocument doc;

    try {
        tinyxml2::XMLError eResult =
            //needs to be deduced with entity strings
            doc.LoadFile(std::string(playlistDir + loadedPlaylist + std::string(".xspf")).c_str());

        LOG(INFO) << std::string("Playlist found at: " + playlistDir + loadedPlaylist + std::string(".xspf"));

        if(eResult not_eq tinyxml2::XML_SUCCESS) {
            throw std::string("\033[2;31mError: XML file \033[0m" + std::string(
                                  playlistDir + loadedPlaylist + std::string(".xspf")) +
                              "\033[2;31mdoes not exist or is corrupt (like Hillary Clinton)\033[0m");
        }
    } catch(const std::string e) {
        doc.PrintError();
        LOG(ERROR) << e;
        LOG(ERROR) << "DMUX is going to have to randomly select music for you to play, please locate documentation on how to recreate your playlists for this scene. This error WILL NOT be solved by restarting the game.";
        LOG(ERROR) << "If you can replicate this error please email the DMUX developers at bkeys@gnu.org with the error message, your operating system, and version of DMUX you have installed";
        return;
    }

    tinyxml2::XMLElement *root =
        doc.FirstChildElement("playlist")->FirstChildElement("trackList");

    for(tinyxml2::XMLElement *e = root->FirstChildElement("track"); e != nullptr;
            e = e->NextSiblingElement("track")) {

        Record newRecord;

        newRecord.location = fetchRelPath(e->FirstChildElement("location")->GetText());
        replaceString(newRecord.location, "%20", " "); // handles spaces
        replaceString(newRecord.location, "%28", "("); // handles (
        replaceString(newRecord.location, "%29", ")"); // handles )
        replaceString(newRecord.location, "%amp;", "&"); // handles ;

        newRecord.title = fetchRelPath(e->FirstChildElement("title")->GetText());
        newRecord.creator = fetchRelPath(e->FirstChildElement("creator")->GetText());
        records.push_back(newRecord);
    }

    std::string filePath = std::string(playlistDir + loadedPlaylist +
                                       std::string(".xspf"));
    changePlaylist = false;
}

void JukeBox::setPlayList(const char *pl) {
    records.clear();
    loadedPlaylist = std::string(pl);
    changePlaylist = true;
}

JukeBox::JukeBox() {

    closed = false;
    audioMgr = cAudio::createAudioManager(false, std::string(logDir + "Dmux_AudioLog.html").c_str());
#ifndef NDEBUG
    assert(audioMgr not_eq nullptr);
#endif

    if(audioMgr) {
        cAudio::IAudioDeviceList *pDeviceList = cAudio::createAudioDeviceList();
#ifndef NDEBUG
        assert(pDeviceList not_eq nullptr);
#endif
        cAudio::cAudioString defaultDeviceName = pDeviceList->getDefaultDeviceName();
        audioMgr->initialize(pDeviceList->getDefaultDeviceName().c_str());
        CAUDIO_DELETE pDeviceList;
        pDeviceList = 0;
    } else {
        LOG(ERROR) << "Failed to create audio playback audioMgr.";
    }

    readPlayListXML();
}

void JukeBox::jukeBoxLoop() {

    while(not closed) {
        playSong();
    }
    closed = true;
}

void JukeBox::playFX(const char *s) {

#ifndef NDEBUG
    assert(s not_eq nullptr);
#endif

    if(sfx.size() > 15) {
        return;
    }

    sfx.push_back(std::thread([s]() {
        std::string soundEffect(sfxDir + s);
        //Create an IAudio object and load a sound from a file
        cAudio::IAudioSource *mySound = audioMgr->create("effect", soundEffect.c_str(),
                                        true);
#ifndef NDEBUG
        assert(mySound not_eq nullptr);
#endif
        if(mySound and not closed) {
            mySound->play2d(false);

            while(mySound->isPlaying()) {
                mySound->setVolume(masterVolume * soundfxVolume);
                cAudio::cAudioSleep(10);
                masterVolume = static_cast<irr::f32>(Game::playerSettings.getMasterVolume() / irr::f32(100.0f));
                soundfxVolume = static_cast<irr::f32>(Game::playerSettings.getSoundVolume() / irr::f32(100.0f));
            }
        }
    }));
}

void JukeBox::playSong() {
    std::random_device r;
    // Choose a random mean between 1 and 6
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(0, records.size() - 1);
    int selection = uniform_dist(e1);

    cAudio::IAudioSource *mySound = audioMgr->create("song", std::string(musicDir + records[selection].location).c_str(), true);

#ifndef NDEBUG
    assert(mySound not_eq nullptr);
#endif

    if(mySound) {
        mySound->play2d(false);

        irr::f32 volumeModifier = 0; // amplifies for the fade in/out

        LOG(INFO) << std::string("Played Song : " + records[selection].title);
        while(mySound->isPlaying() and mySound->isValid() and not closed and not changePlaylist) {
            musicVolume = static_cast<irr::f32>(Game::playerSettings.getMusicVolume() / irr::f32(100.0f));
            masterVolume = static_cast<irr::f32>(Game::playerSettings.getMasterVolume() / irr::f32(100.0f));

            if(mySound->getCurrentAudioTime() < 5) {
                volumeModifier = (0.04 * (pow((mySound->getCurrentAudioTime()), 2)));
            } else if(mySound->getCurrentAudioTime() > mySound->getTotalAudioTime() - 5) {
                volumeModifier = (fabs((-0.04 * (pow((mySound->getCurrentAudioTime() -
                                                      mySound->getTotalAudioTime()), 2)))));
            }
            mySound->setVolume(masterVolume * (musicVolume * volumeModifier));
            cAudio::cAudioSleep(10);
        }
    }
}

/* Safely destroys the JukeBox and it's internals */
JukeBox::~JukeBox() {
    std::for_each(sfx.begin(), sfx.end(), [](std::thread & t) {
        t.join();
    });
    audioMgr->shutDown();
    cAudio::destroyAudioManager(audioMgr);
}
