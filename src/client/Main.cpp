#include <stdlib.h>
#include <time.h>
#include "Game.hpp"
#include <easylogging++.h>
#include <getopt.h>

int main(int argc, char *argv[]) {
    START_EASYLOGGINGPP(argc, argv);
    srand(time(nullptr));
    
    /*
    // opens 'irrlicht.log' for writing and maps stdout to that file
    FILE *log = freopen(std::string(cpplocate::findModule("dmux").value("logDir") + "Dmux_GameLogs.txt").c_str(), "w", stdout);
    setvbuf(log, 0, _IONBF, 0); // disable buffering
    */

    return Game()();
}
