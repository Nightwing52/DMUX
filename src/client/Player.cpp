#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <easylogging++.h>
#ifndef NDEBUG
#include <cassert>
#endif

#include "Player.hpp"
#include "Game.hpp"
#include "Simulation.hpp"
#include "Observer.hpp"
#include "gui/Menu.hpp"
#include "Arena.hpp"
#include "PacketTypes.hpp"

// Because fucking Windows doesn't have fucking *PI*!!!!
#ifndef M_PI
#define M_PI 3.14159265358979323
#endif //M_PI

btScalar yaw;
btScalar pitch;

Player::Player() :
    Gui(),
    Observer(),
    isTeamSelected(false), //used for team selection
    v(Arena::getSpawnPoint()),
    isInPauseMenu(false),
    oldIsInPauseMenu(false),
    isInChat(false) {

    chatText.push_back(std::string("Chat Text Here: "));

    //settings available teams, eventually we will
    //get these in the form of a packet from the server
    availableTeams.push_back("Fascist");
    availableTeams.push_back("Communist");
    availableTeams.push_back("Feminist");

    for(int i = 0; i < irr::KEY_KEY_CODES_COUNT; ++i) {
        keyIsDown[i] = false;
    }

    for(int i = 0; i < NUM_ACTION_KEYS; ++i) {
        actionKeysDown[i] = oldActionKeysDown[i] = false;
    }

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
#endif //NDEBUG

    camera = Game::device->getSceneManager()->addCameraSceneNode();
    camera->setPosition(irr::core::vector3df(0, 30, 200));
    lastSendTime = std::clock();
    Game::sim.add_observer(this);
    imguiSettings->mIsGUIMouseCursorEnabled = false;

    // Clean message
    for(unsigned short i = 0; i < sizeof(chatMsg) / sizeof(char); ++i) {
        chatMsg[i] = 0;
    }
}

void Player::show() {
    pGUI->startGUI();

    //At some point we want to remove the system cursor all together
    //and only use a special in game cursor
    // NOTE: what you said here should be an option by the user. Game
    // cursors can be slow and annoying (from personal experience).
    if(not isTeamSelected) {
        isTeamSelected = Game::menu->showTeamSelection();
    } else if(isInPauseMenu) {
        isInPauseMenu = Game::menu->showPauseMenu();
    }

    const unsigned int windowX = Game::playerSettings.set.currentWindowSize.first;
    const unsigned int windowY = Game::playerSettings.set.currentWindowSize.second;

    // Create the chat window
    ImGui::SetNextWindowPos(ImVec2(0, (windowY << 1) / 3));
    ImGui::SetNextWindowSize(ImVec2((windowX << 2) / 9, windowY / 3));
    ImGui::Begin("Chat", nullptr, ImGuiWindowFlags_NoTitleBar);
    ImGui::Text("Chat:");
    ImGui::Separator();
    // Show chat messages:
    ImGui::BeginChild("ScrollingRegion",
                      ImVec2(0, -ImGui::GetItemsLineHeightWithSpacing()),
                      false, ImGuiWindowFlags_HorizontalScrollbar);
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1));
    for(unsigned int i = 0; i < chatText.size(); ++i) {
        ImGui::Text("%s", chatText[i].c_str());
    }
    ImGui::SetScrollHere();
    ImGui::PopStyleVar();
    ImGui::EndChild();
    ImGui::Separator();
    ImGui::InputText("", chatMsg, 256);
    if(actionKeysDown[ENABLE_CHAT] and not isInChat and
            not ImGui::IsItemActive()) {
        ImGui::SetKeyboardFocusHere();
        actionKeysDown[ENABLE_CHAT] = false;
        isInChat = true;
    } else if(not actionKeysDown[ENABLE_CHAT] and
              not ImGui::IsItemActive() and isInChat) {
        isInChat = false;
        Game::device->setEventReceiver(this);
    }
    ImGui::End();

    pGUI->drawAll();
}

bool Player::OnEvent(const irr::SEvent &event) {
    switch(event.EventType) {
        case irr::EET_KEY_INPUT_EVENT:
            keyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
            return true;
        case irr::EET_LOG_TEXT_EVENT:
            LOG(INFO) << std::string(event.LogEvent.Text);
            return true;
        default:
            break;
    }
    return false;
}

bool Player::isKeyDown(const irr::EKEY_CODE &keyCode) const {
    return keyIsDown[keyCode];
}

void Player::notify() {
    if(isInChat or isInPauseMenu or not isTeamSelected) {
        setGuiListener();
    } else {
        Game::device->setEventReceiver(this);
    }

    moveCameraControl();

    {
        btScalar interval = (std::clock() - lastSendTime) /
                            static_cast<double>(CLOCKS_PER_SEC);
        if(interval >= 0.03) {
            // TODO: Send the cam rotation
        }
    }

    if(isKeyDown(irr::KEY_ESCAPE) and oldIsInPauseMenu == isInPauseMenu) {
        if(isInPauseMenu) {
            isInPauseMenu = false;
            return;
        } else {
            isInPauseMenu = true;
        }
    } else if(not isKeyDown(irr::KEY_ESCAPE) and oldIsInPauseMenu not_eq isInPauseMenu) {
        oldIsInPauseMenu = isInPauseMenu;
    }

    updateActionKeys();

    {
        btScalar accelForce = 0.0f;
        accelForce += actionKeysDown[ACCEL_FORWARD] ? 10000.0f : 0;
        accelForce -= actionKeysDown[ACCEL_REVERSE] ? 10000.0f : 0;
        v.setEngineForce(accelForce);

        btScalar steer = 0.0f;
        steer += actionKeysDown[STEER_RIGHT] ? 0.01f : 0;
        steer -= actionKeysDown[STEER_LEFT] ? 0.01f : 0;
        v.updateTires(steer);
    }

    //if(actionKeysDown[ENABLE_CHAT] and not isInChat) isInChat = true;

    /*if(isKeyDown(irr::KEY_RETURN) and isInChat) {
        std::cout << "Returning chat works.\n";
        // Create packet and send the message
        Game::client.send(createChatMessagePacket(chatMessage.c_str()));
        chatMessage.clear();
        Game::device->setEventReceiver(this);
        isInChat = false;
    }*/

    // If the action keys pressed have changed then send a packet
    for(unsigned short i = 0; i < NUM_ACTION_KEYS; ++i) {
        if(oldActionKeysDown[i] not_eq actionKeysDown[i]) {
            for(unsigned short i = 0; i < NUM_ACTION_KEYS; ++i) {
                oldActionKeysDown[i] = actionKeysDown[i];
            }
            break;
        }
    }
}

inline void Player::updateActionKeys() {
    // TODO: Work on making the action keys pointers to the literal keys
    actionKeysDown[ACCEL_FORWARD] = isKeyDown(irr::KEY_KEY_W);
    actionKeysDown[ACCEL_REVERSE] = isKeyDown(irr::KEY_KEY_S);
    actionKeysDown[STEER_LEFT] = isKeyDown(irr::KEY_KEY_A);
    actionKeysDown[STEER_RIGHT] = isKeyDown(irr::KEY_KEY_D);
    actionKeysDown[HEADLIGHT_TOGGLE] = isKeyDown(irr::KEY_KEY_F);
    actionKeysDown[ENABLE_CHAT] = isKeyDown(irr::KEY_KEY_T);
}

Player::~Player() { }

inline void Player::moveCameraControl() const {
    if(Game::device->isWindowActive() and
            not isInPauseMenu and
            isTeamSelected and
            not isInChat) {
        // Get the cursor's current position
        irr::core::position2d<irr::f32> cursorPos = Game::device->getCursorControl()->getRelativePosition();

        // Check to see how much its position has changed
        btScalar changeX = (cursorPos.X - 0.5) * Game::playerSettings.set.mouseSensitivity * 3;
        btScalar changeY = (cursorPos.Y - 0.5) * Game::playerSettings.set.mouseSensitivity * 3;

        // Move the cursor back to (.5, .5)
        Game::device->getCursorControl()->setPosition(0.5f, 0.5f);

        // Change the yaw & pitch based on the change in X & Y of the cursor
        yaw += changeX;
        pitch += changeY;

        // Set limits to pitch
        if(pitch < -45) {
            pitch = -45;
        } else if(pitch > 45) {
            pitch = 45;
        }

        // We don't want any numbers lower than 0 or higher than 360
        if(yaw > 360) {
            yaw -= 360;
        } else if(yaw < 0) {
            yaw += 360;
        }
    }

    // Get the vehicle position
    btVector3 pos = v.physics.rigidBody->getCenterOfMassPosition();
    const irr::core::vector3df playerPos = irr::core::vector3df(pos[0], pos[1], pos[2]);

    // Calculate where the camera should be placed (yaw and pitch based)
    btScalar xf = playerPos.X - (sin(yaw * M_PI / 180.0f) *
                                 cos(pitch * M_PI / 180.0f)) * 16.0f; // NOTE: 16.0f is the distance
    btScalar yf = playerPos.Y + sin(pitch * M_PI / 180.0f) * 16.0f;
    btScalar zf = playerPos.Z - (cos(yaw * M_PI / 180.0f) *
                                 cos(pitch * M_PI / 180.0f)) * 16.0f;
    camera->setPosition(irr::core::vector3df(xf, yf, zf));
    // Make the camera look at the vehicle
    camera->setTarget(playerPos);
}
