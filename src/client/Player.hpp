#pragma once

/**
 * @file   Player.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @author Nicolas Ortega (deathsbreed@themusicinnoise.net)
 * @brief  Player handles all the direct interactions the
 * human playing DMUX
 * Copyright (c) 2016 Collective Tyranny
 */

#include <clocale>
#include <locale>
#include <irrlicht.h>
#include <ctime>
#include <vector>
#include "Vehicle.hpp"
#include "PacketTypes.hpp"
#include "Arena.hpp"
#include "gui/Gui.hpp"
#include "Observer.hpp"
//#include "obs/observable.h"

/**
 * \brief Handles interaction with the player of DMUX
 */
class Player final : public core::Gui, public core::Observer, public irr::IEventReceiver {
public:

    /**
     * \brief Initializes the player and it's elements
     * \details Constructor:
     * - Places the player inside of a world
     * - Initializes their vehicle and places it in the world
     * - Initializes the light node (needs to be moved to vehicle)
     * - Initialize the camera
     */
    Player();
    /**
     * \brief Draw all GUI elements in the game.
     * \details Draws the GUI elements for team select, pause menu, and so on.
     */
    void show();
    ~Player();

    irr::scene::ICameraSceneNode *camera; //!< Game camera for Player

    /**
     * \brief Notify function mandatory for Observer classes
     * \details Does the following each frame:
     * - Checks if the window is active or game paused
     * - Handles the headlights (needs to be in Vehicle class)
     * - Checks to see if the user is quitting
     */
    void notify();

    /**
     * \brief function called whenever the player has any kind of input
     * \details Handles events involving the GUI and the gameplay, there
     * is a toggle between modes for the GUI and for the gameplay so the
     * handling of events are independant for both
     * \param event The player event detected by Irrlicht
     */
    bool OnEvent(const irr::SEvent &event);

    /**
     * \brief test whether a key is down
     * \param keyCode The keycode being checked to see if it is active
     * \returns whether the key is pressed
     */
    bool isKeyDown(const irr::EKEY_CODE &keyCode) const;

private:

    bool isTeamSelected;
    std::vector<std::string> availableTeams;

    /**
     * \brief Array holding all the activated keys
     */
    bool keyIsDown[irr::KEY_KEY_CODES_COUNT];

    /**
     * \brief Array of keys pressed.
     * \details An array of all the game keys pressed that will later be sent to the server as input.
     */
    bool actionKeysDown[NUM_ACTION_KEYS];
    /**
     * \brief Used to see if actionKeysDown has changed.
     * \details This array is compared to actionKeysDown to see if it was changed in order to send it to the server.
     */
    bool oldActionKeysDown[NUM_ACTION_KEYS];

    /**
     * \brief Apply input to actionKeysDown.
     * \details Apply the game's input converting the key presses of buttons into an action that other hosts can understand.
     */
    inline void updateActionKeys();
    inline void moveCameraControl() const; //!< Adjusts our third person camera
    std::string teamAlignment; //!< Which team the player is on (if any)
    Vehicle v; //!< Players vehicle they control for game play
    bool isInPauseMenu; //!< if player is in pause menu
    bool oldIsInPauseMenu; //!< Used to avoid flickering pause menu
    bool isInChat; //!< if in game chat is enabled
    irr::scene::ILightSceneNode *lights; //!< Light node for headlights
    clock_t lastSendTime; //!< regulates the speed at which we send packets
    //std::string chatMessage; //!< Chat message user inputs
    char chatMsg[256];
    std::vector<std::string> chatText; //<! Contents of the chat box
};
