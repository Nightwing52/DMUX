#include "comp/Physics.hpp"
#include "Simulation.hpp"
#include <LinearMath/btDefaultMotionState.h>
#include <LinearMath/btMotionState.h>
#include <btBulletDynamicsCommon.h>
namespace core {
namespace comp {
Physics::~Physics() {
    Simulation::mir.collisionShapes.remove(shape);
    delete motionState;
    delete shape;
    delete rigidBody;
}
} // comp
} // core
