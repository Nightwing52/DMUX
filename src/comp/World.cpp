#include "World.hpp"
#include "Simulation.hpp"
#include "sys/World.hpp"
#include "Observer.hpp"

#include <btBulletDynamicsCommon.h>

#include <cstddef>
#include <string>

namespace core {
namespace comp {

World::~World() {
    //delete collision shapes
    for(int j = 0; j < collisionShapes.size(); j++) {
        btCollisionShape *shape = collisionShapes[j];
        if(shape == nullptr) {
            delete shape;
        }
    }
    collisionShapes.clear();
    delete world;
    delete solver;
    delete broadphase;
    delete dispatcher;
    delete collisionConfiguration;
}
} // comp
} // core
