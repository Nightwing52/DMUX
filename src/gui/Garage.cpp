#include <sstream>
#include <fstream>
#include <array>
#include <string>
#include <iostream>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>
#include <IrrIMGUI/IncludeIrrlicht.h>
#include <IrrIMGUI/IncludeIMGUI.h>
#include <IrrIMGUI/IrrIMGUI.h>
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <LinearMath/btVector3.h>
#include <easylogging++.h>

#include "Layout.hpp"
#include "DmuxCommon.hpp"
#include "Garage.hpp"
#include "client/Game.hpp"
#include "sys/World.hpp"
#include "sys/XmlParser.hpp"

btScalar chassisRotation = 180.0f;
btScalar cameraDisplacement = 0.0f;
btScalar rotationDirection = 1.0f;

unsigned short componentSelection = 0;
int chassisSelection = 0;
int tireSelection = 0;
static int layoutSelection = 0;
bool isInRenameLayoutPrompt = false;
std::string renameButtonText; // The button text changes depending on whether the layout is being renamed
static std::array<char, 32> layoutName;
static const std::string confDir = cpplocate::findModule("dmux").value("confDir");

namespace menu {

std::vector<Layout> Garage::availableLayouts; //!< Layouts the user has saved and are available for display
Garage::Garage() :
    Gui(),
    mainScreen(Game::device->getSceneManager()->addEmptySceneNode()),
    layoutScreen(Game::device->getSceneManager()->addEmptySceneNode()),
    mainCamera(Game::device->getSceneManager()->addCameraSceneNode(mainScreen, irr::core::vector3df(0), irr::core::vector3df(0, 0, 5))),
    layoutCamera(Game::device->getSceneManager()->addCameraSceneNode(layoutScreen, irr::core::vector3df(0, 0, 15), irr::core::vector3df(0, 0, 25))),
    // Preparing the render targets for double rendering
    renderTarget(Game::device->getVideoDriver()->addRenderTargetTexture(irr::core::dimension2d<irr::u32>(screenWidth(0.45f), screenHeight(0.25f)), "Layout")),
    renderTextureID(pGUI->createTexture(renderTarget)) {

    updateAvailableLayouts(); // Scan all layout files and add them to availableLayouts

    loadAvailableComponents(); // Scans the assets folder and gives us all available components

    // Creating the background
    Game::device->getVideoDriver()->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);
    const std::string background = cpplocate::findModule("dmux").value("assetsDir") + "Garage.jpg";

    // Consider replacing this with a billboard
    displayBackground = Game::device->getSceneManager()->addSkyBoxSceneNode(Game::device->getVideoDriver()->getTexture(background.c_str()),
                                                Game::device->getVideoDriver()->getTexture(background.c_str()),
                                                Game::device->getVideoDriver()->getTexture(background.c_str()),
                                                Game::device->getVideoDriver()->getTexture(background.c_str()),
                                                Game::device->getVideoDriver()->getTexture(background.c_str()),
                                                Game::device->getVideoDriver()->getTexture(background.c_str()));
    Game::device->getVideoDriver()->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, true);

    displayBackground->setVisible(false);

    displayLight = Game::device->getSceneManager()->addLightSceneNode(0, irr::core::vector3df(0,3,10),
                                                                      irr::video::SColorf(1, 1, 1, 0.5f), 800.0f);
    displayLight->setVisible(true);

    // If there are no saved layouts then create a default one and add it
    if(availableLayouts.empty()) {
        addCurrentlySelectedLayout();
    }

    preLoadLayoutNodes();
    alignSelectionToLayout(); // Get the selections in the GUI ready to be shown
    alignLayoutToSelection(); // Somehow keeps the wheels in the right spot when we start the Garage, bkeys has no clue how.
}

Layout Garage::getDefaultLayout() {
    for(const auto &layout : availableLayouts) {
        if(layout.isDefault) {
            return layout;
        }
    }

    // Exception, we will just give them the layout they had previously selected
    return availableLayouts[layoutSelection];
}

void Garage::preLoadLayoutNodes() {

    // Get all available chassis nodes from the assets folder
    for(unsigned int i = 0; i < availableChassis.size(); ++i) {
        chassisNodes.push_back(Game::device->getSceneManager()->addMeshSceneNode(Game::device->getSceneManager()->getMesh(std::string(cpplocate::findModule("dmux").value("chassisDir") + availableChassis[i]["objStr"] + "/" + availableChassis[i]["objStr"] + ".b3d").c_str())));
        chassisNodes[i]->setPosition(irr::core::vector3df(0, -2, 21));
        chassisNodes[i]->setMaterialFlag(irr::video::EMF_LIGHTING, true);
        chassisNodes[i]->setVisible(false);
    }

    // Get all available tire nodes from the assets folder
    for(unsigned int i = 0; i < availableTires.size(); ++i) {
        std::vector<irr::scene::IMeshSceneNode *> tireSet;
        for(int j = 0; j < 4; ++j) {
            tireSet.push_back(Game::device->getSceneManager()->addMeshSceneNode(Game::device->getSceneManager()->getMesh(std::string(cpplocate::findModule("dmux").value("tireDir") + availableTires[i]["objStr"] + "/" + availableTires[i]["objStr"] + ".b3d").c_str())));
            tireSet[j]->setPosition(irr::core::vector3df(0, -2, 21));
            tireSet[j]->setMaterialFlag(irr::video::EMF_LIGHTING, true);
            tireSet[j]->setVisible(false);
        }
        tireNodes.push_back(tireSet);
    }
}

void Garage::loadAvailableComponents() {
    // Get all of the metadata for available tires and chassis from our assets folder
    availableChassis = core::sys::getSceneMetaDataFromDir(cpplocate::findModule("dmux").value("chassisDir"));
    availableTires = core::sys::getSceneMetaDataFromDir(cpplocate::findModule("dmux").value("tireDir"));
}

Garage::~Garage() {
    pGUI->deleteTexture(renderTextureID);
}

void Garage::show() {

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    // Creating our IMGUI window
    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
    ImGui::SetNextWindowSize(ImVec2(screenWidth(1),
                                    screenHeight(0.9f)));

    // Settings style variables for our window
    ImGui::Begin("Garage", nullptr, ImGuiWindowFlags_NoTitleBar);
    ImGui::PushFont(font["regular"]);
    ImGui::Columns(2);

    displayBackground->setVisible(true);
    displayCarWidgets();
    displayBackground->setVisible(false);
    displayStatBars();

    ImGui::NextColumn();

    // These if statements are true if the listbox selection was changed
    ImGui::BeginChild("##tabs", ImVec2(-1, ImGui::GetWindowHeight() / 20));
    ImGui::Columns(3, nullptr, false);
    std::vector<std::string> categories;
    categories.push_back("Chassis");
    categories.push_back("Tires");
    categories.push_back("Weapons");

    for(unsigned int i = 0; i < categories.size(); i++) {
        if(i == componentSelection) {
            if(ImGui::Selectable(categories[i].c_str(), true)) {}
        } else {
            if(ImGui::Selectable(categories[i].c_str(), false)) {
                componentSelection = i;
            }
        }
        ImGui::NextColumn();
    }
    ImGui::Columns(1);
    ImGui::Separator();

    ImGui::EndChild();

    // loading the GUI for the chosen category
    switch(componentSelection) {
    case 0: //chassis
        if(availableChassisWidgets()) {
            alignLayoutToSelection();
            isInRenameLayoutPrompt = false;
        }
        break;
    case 1: //tires
        if(availableTiresWidgets()) {
            alignLayoutToSelection();
            isInRenameLayoutPrompt = false;
        }
        break;
    case 2: // weapons

        break;
    }

    if(availableLayoutWidgets()) {
        alignSelectionToLayout();
        isInRenameLayoutPrompt = false;
    }
    ImGui::SameLine();
    ImGui::NextColumn();
    ImGui::BeginChild("Modify buttons", ImVec2(-1, -1));
    ImGui::Separator();
    ImGui::NewLine();
    renameLayoutPromptWidgets();
    ImGui::SameLine();
    if(ImGui::Button("New Layout", ImVec2((ImGui::GetWindowWidth() / 2), ImGui::GetWindowHeight() / 3))) {
        addCurrentlySelectedLayout();
    }

    if(ImGui::Button("Clear All Layouts", ImVec2((ImGui::GetWindowWidth() / 2), ImGui::GetWindowHeight() / 3))) {
        deleteAllSavedLayouts();
    }

    ImGui::SameLine();
    if(ImGui::Button("Set Default Layout", ImVec2((ImGui::GetWindowWidth() / 2), ImGui::GetWindowHeight() / 3))) {
        setSelectedLayoutAsDefault();
    }

    ImGui::EndChild();

    ImGui::NextColumn();

    ImGui::PopFont();
    ImGui::End();
    ImGui::PopStyleVar();
}

void Garage::addCurrentlySelectedLayout() {
    Layout l;
    l.name = "Layout" + std::to_string(availableLayouts.size());
    l.setChassis(availableChassis[chassisSelection]["objStr"]);
    l.setTire(availableTires[tireSelection]["objStr"]);

    // There needs to be a default car
    if(availableLayouts.size() == 0) {
        l.isDefault = true;
    }
    std::ofstream outputStream(confDir + "layout" + std::to_string(availableLayouts.size()), std::ios::binary);
    cereal::PortableBinaryOutputArchive archive(outputStream);
    archive(l);
    availableLayouts.push_back(l);
}

// This function is executed when no listbox widgets have been interacted with
void Garage::alignLayoutToSelection() {

    // Align the current layout to the currently selected components

    // Consider using a modal widget for this
    // Renaming functionality was a bit tricky, we have to know if we are in the
    // rename prompt first.
    if(isInRenameLayoutPrompt == true) {

        // If we are in the rename prompt; then we use the data in the textinput
        // This contains the name the user specified for their layout
        availableLayouts[layoutSelection].name = layoutName.data();
    } else {

        // If not, then the user is selecting a different component for their layout
        Layout l;
        std::ifstream inputStream(confDir + "layout" + std::to_string(layoutSelection), std::ios::binary);
        cereal::PortableBinaryInputArchive archive(inputStream);
        archive(l);
        // Meaning that they do not want the name to change, so we deserialize the
        // current layout and get the name and assign it to the newly constructed layout
        availableLayouts[layoutSelection].name = l.name;
    }
    availableLayouts[layoutSelection].setChassis(availableChassis[chassisSelection]["objStr"]);
    availableLayouts[layoutSelection].setTire(availableTires[tireSelection]["objStr"]);

    // Now save the adjusted layout to the file
    std::ofstream outputStream(confDir + "layout" + std::to_string(layoutSelection), std::ios::binary);
    cereal::PortableBinaryOutputArchive archive(outputStream);
    archive(availableLayouts[layoutSelection]);
}

void Garage::alignSelectionToLayout() {

    // The user selected a new layout, align the selection to the newly selected layout
    std::ifstream inputStream(confDir + "layout" + std::to_string(layoutSelection), std::ios::binary);
    cereal::PortableBinaryInputArchive archive(inputStream);
    archive(availableLayouts[layoutSelection]);

    // Align the chassis selection
    for(unsigned int i = 0; i < availableChassis.size(); ++i) {
        // when the selection's name and the selected chassis name are the same, align
        // the two to eachother
        if(availableChassis[i]["Name"] == availableLayouts[layoutSelection].getChassis()["Name"]) {
            chassisSelection = i;
        }
    }
    // Align the tire selection
    for(unsigned int i = 0; i < availableTires.size(); ++i) {
        // when the selection's name and the selected tires name are the same, align
        // the two to eachother
        if(availableTires[i]["Name"] == availableLayouts[layoutSelection].getTire()["Name"]) {
            tireSelection = i;
        }
    }
}

void Garage::updateAvailableLayouts() {
    availableLayouts.clear();
    Layout l;
    for(unsigned int i = 0; fileExists(confDir + "layout" + std::to_string(i)); ++i) {
        std::ifstream inputStream(confDir + "layout" + std::to_string(i), std::ios::binary);
        cereal::PortableBinaryInputArchive archive(inputStream);
        archive(l);
        availableLayouts.push_back(l);
    }
}

void Garage::renameLayoutPromptWidgets() {

    // Using the same button for either case of it being renamed or not.
    // But the content of the button can be either string and if the layout
    // is being renamed then draw a text input box so the user knows it works.
    if(isInRenameLayoutPrompt == true) {
        ImGui::PushItemWidth((ImGui::GetWindowWidth() / 2));
        ImGui::InputText("Layout Name", layoutName.data(), layoutName.size());
        ImGui::PopItemWidth();
        renameButtonText = "Apply Name";
    } else {
        renameButtonText = "Rename Layout";
    }

    if(ImGui::Button(renameButtonText.c_str(), ImVec2((ImGui::GetWindowWidth() / 2), ImGui::GetWindowHeight() / 3 ))) {
        if(isInRenameLayoutPrompt == true) {
            alignLayoutToSelection();
        }
        // The button toggles between states
        isInRenameLayoutPrompt = not isInRenameLayoutPrompt;
    }
}

bool Garage::availableChassisWidgets() {
    std::vector<std::string> chassisNames;
    for(auto &chassis : availableChassis) {
        chassisNames.push_back(chassis["Name"]);
    }

    ImGui::BeginChild("Chassis", ImVec2(-1, ImGui::GetWindowHeight() / 2));
    ImGui::PushItemWidth(-1);
    if(ImGui::ListBox("##Available Chassis", &chassisSelection, toSharedPointer(chassisNames).get(), chassisNames.size())) {
        ImGui::PopItemWidth();
        ImGui::EndChild();
        return true;
    }
    ImGui::PopItemWidth();
    ImGui::EndChild();
    return false;
}

bool Garage::availableTiresWidgets() {

    std::vector<std::string> tireNames;
    // Packing the name from the metadata into a vector of strings for the listbox
    for(auto &tire : availableTires) {
        tireNames.push_back(tire["Name"]);
    }
    ImGui::BeginChild("Tires", ImVec2(-1, ImGui::GetWindowHeight() / 2));
    ImGui::PushItemWidth(-1);
    if(ImGui::ListBox("##Available Tire Sets", &tireSelection, toSharedPointer(tireNames).get(), tireNames.size())) {
        ImGui::PopItemWidth();
        ImGui::EndChild();
        return true;
    }
    ImGui::PopItemWidth();
    ImGui::EndChild();
    return false;
}

bool Garage::availableLayoutWidgets() {
    std::vector<std::string> availableLayoutNames;
    // Packing the name from the metadata into a vector of strings for the listbox
    for(const auto &layout : availableLayouts) {
        if(layout.isDefault) {
            availableLayoutNames.push_back(layout.name + " *");
        } else {
            availableLayoutNames.push_back(layout.name);
        }
    }

    ImGui::Text("Saved Layouts");
    ImGui::BeginChild("Layouts", ImVec2(-1, -1));
    ImGui::PushItemWidth(-1);
    ImGui::Separator();
    if(ImGui::ListBox("##Available Layouts", &layoutSelection, toSharedPointer(availableLayoutNames).get(), availableLayoutNames.size())) {
        ImGui::PopItemWidth();
        ImGui::EndChild();
        return true;
    }
    ImGui::PopItemWidth();
    ImGui::EndChild();
    return false;
}

void Garage::displayStatBars() {

    //Simply iterate through the computed stats and display progress bars for them
    btScalar lineHeight = ImGui::GetTextLineHeight();
    ImGui::BeginChild("Names", ImVec2(ImGui::GetWindowWidth() / 8, ImGui::GetWindowHeight() / 4));
    for(const auto &stat : availableLayouts[layoutSelection].generateStats()) {
        ImGui::Text("%s", stat.first.c_str()); // Name of the stat
    }
    ImGui::EndChild();

    ImGui::SameLine();

    ImGui::BeginChild("Value", ImVec2(-1, ImGui::GetWindowHeight() / 4));
    for(const auto &stat : availableLayouts[layoutSelection].generateStats()) {
        btScalar statValue = stat.second;
        if(stat.first == "Weight") {
            statValue /= 1000;
        }
        ImGui::PushStyleColor(ImGuiCol_PlotHistogram, ImColor(1 / statValue, statValue, .0f));
        ImGui::ProgressBar(statValue, ImVec2(-1, lineHeight), "##Horizontal Rotation");
        ImGui::PopStyleColor();
    }
    ImGui::EndChild();
}

void Garage::displayCarWidgets() {

    renderGarageDisplayWidget();

    // Make all nodes invisible by default we only want selected nodes to be visible
    for(const auto &chassisNode : chassisNodes) {
        chassisNode->setVisible(false);
    }

    for(const auto &tireSet : tireNodes) {
        for(const auto &tireNode : tireSet) {
            tireNode->setVisible(false);
        }
    }

    alignTireToChassis();

    // We rotate it extra so that the front of the vehicle faces the user
    if(chassisRotation > 540) {
        rotationDirection = -1.0f;
    } else if(chassisRotation < 180) {
        rotationDirection = 1.0f;
    }

    chassisRotation += (rotationDirection * 0.025f);

    chassisNodes[chassisSelection]->setRotation(irr::core::vector3df(0, chassisRotation, 0));

    // Render the rendered texture to the widget for a 3D preview of the layout
    ImGui::BeginChild("##Car Widget",ImVec2(-1, ImGui::GetWindowHeight() / 2));
    ImGui::ImageButton(renderTextureID, ImVec2(ImGui::GetWindowWidth(), ImGui::GetWindowHeight() / 1.05f));
    if(ImGui::IsItemActive()) {
        // Draw a line between the button and the mouse cursor
        chassisRotation -= ImGui::GetIO().MouseDelta.x;
        cameraDisplacement += (ImGui::GetIO().MouseDelta.y / 40);
        if(cameraDisplacement > 2) {
            cameraDisplacement = 2;
        } else if(cameraDisplacement < -2) {
            cameraDisplacement = -2;
        }
    }
    ImGui::EndChild();
}

void Garage::deleteAllSavedLayouts() {

    for(unsigned int i = 0; i < availableLayouts.size(); ++i) {
        std::remove(std::string(cpplocate::findModule("dmux").value("confDir") + "layout" + std::to_string(i)).c_str());
    }
    updateAvailableLayouts();

    addCurrentlySelectedLayout(); // Gotta put a new layout in place, we must have at least 1 layout available
}

void Garage::alignTireToChassis() {
    alignLayoutToSelection(); // Somehow keeps the wheels in the right spot when we start the Garage, bkeys has no clue how.
    for(unsigned int i = 0; i < availableLayouts[layoutSelection].getWheelPositions().size(); ++i) {
        // Set them to visible and attach it to the chassis
        tireNodes[tireSelection][i]->setVisible(true);
        tireNodes[tireSelection][i]->setParent(chassisNodes[chassisSelection]);
        // We set the position on the chassis's axle
    }
    tireNodes[tireSelection][0]->setPosition(btVectorToIrr(availableLayouts[layoutSelection].getWheelPositions()["fr-wheel"]));

    tireNodes[tireSelection][1]->setPosition(btVectorToIrr(availableLayouts[layoutSelection].getWheelPositions()["fl-wheel"]));
    tireNodes[tireSelection][1]->setRotation(irr::core::vector3df(0.0f, 180.0f, 0.0f));

    tireNodes[tireSelection][2]->setPosition(btVectorToIrr(availableLayouts[layoutSelection].getWheelPositions()["br-wheel"]));

    tireNodes[tireSelection][3]->setPosition(btVectorToIrr(availableLayouts[layoutSelection].getWheelPositions()["bl-wheel"]));
    tireNodes[tireSelection][3]->setRotation(irr::core::vector3df(0.0f, 180.0f, 0.0f));
}

void Garage::renderGarageDisplayWidget() {

    chassisNodes[chassisSelection]->setVisible(true);

    //// Pre render the layout scene

    // Setting the background color for the rendering
    Game::device->getVideoDriver()->setRenderTarget(renderTarget, true, true,
            irr::video::SColor(0, 0, 101, 140));
    layoutScreen->setVisible(true);
    mainScreen->setVisible(false);
    Game::device->getSceneManager()->setActiveCamera(layoutCamera);
    Game::device->getSceneManager()->drawAll();
    Game::device->getVideoDriver()->setRenderTarget(0, true, true, irr::video::SColor(0, 0, 101, 140));

    // Settings the position of the camera
    layoutCamera->setPosition(irr::core::vector3df(layoutCamera->getPosition().X,
                              cameraDisplacement,
                              layoutCamera->getPosition().Z
                                                  ));
    //Render the main scene
    layoutScreen->setVisible(false);
    mainScreen->setVisible(true);
    Game::device->getSceneManager()->setActiveCamera(mainCamera);
}

void Garage::setSelectedLayoutAsDefault() {
    // Set all layouts to not being the default one and save it in the layout files
    for(unsigned int i = 0; i < availableLayouts.size(); ++i) {
        availableLayouts[i].isDefault = false;
        std::ofstream outputStream(confDir + "layout" + std::to_string(i), std::ios::binary);
        cereal::PortableBinaryOutputArchive archive(outputStream);
        archive(availableLayouts[i]);
    }

    availableLayouts[layoutSelection].isDefault = true;
    std::ofstream outputStream(confDir + "layout" + std::to_string(layoutSelection), std::ios::binary);
    cereal::PortableBinaryOutputArchive archive(outputStream);
    archive(availableLayouts[layoutSelection]);
}
}
