#pragma once

/**
 * @file   Garage.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Menu Entry for the Garage, a place where the user
 * can customize their special vehicle they wish to drive in
 * the game. DMUX features a customization system that allows
 * the user to customize their Chassis, Tires, and Weapons to
 * create what is referred to as a Layout (read more at Layout.hpp)
 *
 * KNOWN ISSUES:
 * - The tire's positions are being set every frame, we should be able
 * to set them once during the constructor and only show the tires
 */

#include <string>
#include <vector>

#include "Gui.hpp"

namespace irr {
namespace scene {
    class IMeshSceneNode;
    class ISceneNode;
    class ICameraSceneNode;
}
namespace video {
    class ITexture;
}
}

namespace IrrIMGUI {
    class IGUITexture;
}

class btVector3;
class Layout;

namespace menu {

    /**
     * \brief GUI for customizable vehicles
     * \details In game GUI for customizing vehicle layouts, which are saved in 
     * the conf directory. Of all games in the FOSS world DMUX has the most advanced
     * vehicle customization scheme. The Garage has a 3D preview of the car, and shows
     * all available components that can be applied to the vehicle as well as the 
     * stats for the vehicle.
     */
class Garage final : public core::Gui {
public:
    Garage();
    static Layout getDefaultLayout(); //!< Gets the user's default layout
    ~Garage();
    void show(); //!< mandatory show function called every frame when the menu is active
private:
    static std::vector<Layout> availableLayouts; //!< Layouts the user has saved and are available for display
    std::vector<std::map<std::string, std::string>> availableTires; //!< Meta data of all available tires in the assets directory
    std::vector<std::map<std::string, std::string>> availableChassis; //!< Meta data of all available chassis in the assets directory
    std::vector<irr::scene::IMeshSceneNode *> chassisNodes; //!< Preloaded meshes for all available chassis
    //    std::vector<irr::scene::IMeshSceneNode *> tireNodes; //!< Preloaded meshes for all available tires
    std::vector<std::vector<irr::scene::IMeshSceneNode *>> tireNodes; //!< Preloaded meshes for all available tires

    irr::scene::ISceneNode *const mainScreen;  //!< Used for double rendering
    irr::scene::ISceneNode *const layoutScreen;  //!< Used for double rendering

    irr::scene::ICameraSceneNode *mainCamera; //!< Used for double rendering
    irr::scene::ICameraSceneNode *layoutCamera; //!< Used for double rendering

    irr::video::ITexture *const renderTarget;  //!< Used for double rendering
    IrrIMGUI::IGUITexture *const renderTextureID;  //!< Used for double rendering

    void preLoadLayoutNodes(); //!< Preloads all available meshes for vehicle customization
    void loadAvailableComponents(); //!< Loads all the data about the available components

    /**
     * \brief Takes the current selections, makes them into a layout
     *  and serializes the layout
     * \details Used to create a new default layout
     *  that is aligned with what the user currently has selected.
     *  Afterward the layout is saved to a binary archive and pushed
     *  to the availableLayouts container.
     */
    void addCurrentlySelectedLayout();

    /**
     * \brief Takes the selected layout and modifies the other selection
     *  indexes to what is in that layout
     * \details Executed when the chassis, tires, or weapon listbox has
     *  been interacted with. It is also called when the user renames a layout.
     *  It takes the current selection and serializes it to the layout archive
     */
    void alignLayoutToSelection();

    /**
     * \brief Takes the current selections and aligns it to the layout stored
     * in the file, represented by layoutSelection
     * \details Used when the currently selected layout was switched. Called
     * when the available layouts listbox is interacted with. It deserializes
     * the layout archive, iterates through the available components and sets the
     * selection when the components in the layout and the selection are equal
     */
    void alignSelectionToLayout();

    /**
     * \brief Called when the layout is being serialized so the GUI logic
     * is updated to take the new layout into account
     * \details Clears the availableLayouts container and reinitializes it
     * with the layouts available in the conf directory; used to reread the
     * available layout files in the conf directory
     */
    void updateAvailableLayouts();

    /**
     * \brief Creates IMGUI window for renaming the currently selected vehicle
     * \details Helper function that handles all the widgets and user interaction
     * needed when the user is renaming the currently selected layout
     */
    void renameLayoutPromptWidgets();

    /**
     * \brief Displays the widgets for the available chassis and does event handling
     * \details Helper function that handles all the widgets and user interaction
     * needed when the user is
     */
    bool availableChassisWidgets();

    /**
     * \brief Displays the widgets for the available tires and does event handling
     * \details Helper function that handles all the widgets and user interaction
     * needed when the user is
     */
    bool availableTiresWidgets();
    /**
     * \brief Displays the widgets for the available layotus and does event handling
     * \details Helper function that handles all the widgets and user interaction
     * needed when the user is
     */
    bool availableLayoutWidgets();

    /**
     * \brief Displays the progress bar widgets for the stats of the currently selected
     * layout
     * \details Calls for the layout to compute it's stats and returns it in a
     * container which is iterated through and the values are displayed through
     * IMGUI's progress bar widgets
     */
    void displayStatBars();

    /**
     * \brief Takes the currently selected layout and displays the vehicle on a widget
     * \details Uses the double buffering capabilites of IrrIMGUI and gives us a 3D
     * view of the vehicle that the user has currently selected.
     */
    void displayCarWidgets();

    /**
     * \brief Removes all saved layouts
     * \details Deletes all the player's saved layouts and deletes the files, these
     * files are saved in DMUX's configuration directory (commonly named conf)
     */
    void deleteAllSavedLayouts();

    /**
     * \brief Sets the position of the wheels onto the chassis
     * \details gets the positions from the Layout and aligns the wheels to the chassis
     */
    void alignTireToChassis();

    /**
     * \brief Renders the 3D preview of the customized car in the GUI
     * \details Gives a preview of what the customized car is going to look like inside
     * of the game. Used both for debugging the Layout class and for user enjoyment
     */
    void renderGarageDisplayWidget();

    /**
     * \brief Sets the currently selected layout as the default vehicle
     * \details The Default layout is the layout that is first deployed into battle
     */
    void setSelectedLayoutAsDefault();

    irr::scene::ISceneNode *displayBackground; // Background for layout preview

    irr::scene::ILightSceneNode *displayLight;
};
}
