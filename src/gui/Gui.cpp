#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <IrrIMGUI/IncludeIrrlicht.h>
#include <IrrIMGUI/IncludeIMGUI.h>

#include "Gui.hpp"
#include "client/Game.hpp"

namespace core {

std::map<std::string, ImFont *> Gui::font;

IrrIMGUI::CIMGUIEventReceiver Gui::eventListener;

Gui::Gui() :
    imguiSettings(new IrrIMGUI::SIMGUISettings()),
    fontDir(cpplocate::findModule("dmux").value("fontDir")),
    navWindowSize(std::make_pair(Game::playerSettings.set.currentWindowSize.first,
                                 Game::playerSettings.set.currentWindowSize.second / 10)),
    navWindowPos(std::make_pair(0, Game::playerSettings.set.currentWindowSize.second - (navWindowSize.second))),
    isClosed(false) {

    for(int i = 0; i < irr::KEY_KEY_CODES_COUNT; ++i) {
        keyIsDown[i] = false;
    }

    ImGuiIO &io = ImGui::GetIO();
    io.IniFilename = nullptr;

    // Create a good consistent color scheme and color the
    // files with those, although the GUI is looking great!
    ImGuiStyle gt;
    gt.Colors[ImGuiCol_Text] = ImVec4(1, 1, 1, 1);
    gt.Colors[ImGuiCol_Button] = ImVec4(.1, .1, .1, 1);
    gt.Colors[ImGuiCol_ButtonHovered] = ImVec4(.15, .15, .15, 1);
    gt.Colors[ImGuiCol_WindowBg] = ImVec4(.15, .15, .15, 1);
    gt.Colors[ImGuiCol_PlotHistogram] = ImVec4(.5, .05, .05, 1);

    ImGui::GetStyle() = gt;

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
    assert(imguiSettings not_eq nullptr);
#endif

    pGUI = createIMGUI(Game::device, &eventListener, imguiSettings);

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
    assert(imguiSettings not_eq nullptr);
#endif

    if(font.empty()) {
        //Loading custom fonts for IrrIMGUI
        font["bold"] = pGUI->addFontFromFileTTF(std::string(fontDir + "MerriweatherSans-Bold.ttf").c_str(), (navWindowSize.second / 3));
        font["heavy"] = pGUI->addFontFromFileTTF(std::string(fontDir + "Merriweather-Heavy.ttf").c_str(), (navWindowSize.second / 3));
        font["regular"] = pGUI->addFontFromFileTTF(std::string(fontDir + "MerriweatherSans-Regular.ttf").c_str(), (navWindowSize.second / 3));
    }
    pGUI->compileFonts();

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
    assert(imguiSettings not_eq nullptr);
    assert(pGUI not_eq nullptr);
    for(auto f : font) {
        assert(f.second not_eq nullptr);
    }
#endif
}

void Gui::setGuiListener() {
    Game::device->setEventReceiver(&eventListener);
}

void Gui::closeMenu() {
    isClosed = true;
}
bool Gui::run() {
    return not isClosed;
}

bool Gui::isKeyDown(const irr::EKEY_CODE &keyCode) const {
    return keyIsDown[keyCode];
}

Gui::~Gui() {
    pGUI->drop();
    delete imguiSettings;
}

    // this looks very useful
    // http://www.irrlicht3d.org/wiki/index.php?n=Main.GettingScreenAndWindowResolution

btScalar Gui::screenWidth(const btScalar &percent) {
    return percent * Game::device->getVideoDriver()->getScreenSize().Width;
}

btScalar Gui::screenHeight(const btScalar &percent) {
    return percent * Game::device->getVideoDriver()->getScreenSize().Height;
}

}
