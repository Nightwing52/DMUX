#include <iostream>
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <easylogging++.h>
#include <array>
#include <IrrIMGUI/IncludeIrrlicht.h>
#include <IrrIMGUI/IncludeIMGUI.h>
#include <IrrIMGUI/IrrIMGUI.h>
#include <fstream>

#include "Menu.hpp"
#include "NetCommon.hpp"
#include "client/Game.hpp"
#include "client/JukeBox.hpp"
#include "client/Game.hpp"
#include "sys/XmlParser.hpp"
#include "DmuxCommon.hpp"

//CONTROLS
bool isKeyInputMenuActive = false;
bool isVideoSettingsMenuActive = false;
bool isKeyModifying = false;
int arenaSelection = 0;
int serverSelection = 0;
int selectedPort = 0;
std::array<char, 32> customServer;
std::array<char, 8> customIp;

namespace menu {
Menu::Menu() :
    Gui(),
    garage(),
    selectedWindow(10) { //we want no window selected

    std::ifstream contributorFileStream(cpplocate::findModule("dmux").value("projectDir") + std::string("CONTRIBUTORS.md"));

    while(not contributorFileStream.eof()) {
        std::string data;
        getline(contributorFileStream, data);
        contributorFileContent.push_back(data);
    }
    contributorFileStream.close();
    //Creating menu buttons for main menu
    availableButtons.push_back("Find Game");
    availableButtons.push_back("Garage");
    availableButtons.push_back("Settings");
    availableButtons.push_back("Credits");
    availableButtons.push_back("Exit");

    //options for pause menu
    pauseMenuOptions.push_back("Return to game");
    pauseMenuOptions.push_back("Change Team Alignment");
    pauseMenuOptions.push_back("Settings");
    pauseMenuOptions.push_back("Leave game");
    pauseMenuOptions.push_back("Exit");

#ifndef NDEBUG
    assert(Game::device not_eq nullptr);
#endif
    setGuiListener();
#ifndef NDEBUG
    assert(imguiSettings not_eq nullptr);
    assert(pGUI not_eq nullptr);
#endif

    isKeyModifying = false;

    //settings available teams, eventually we will
    //get these in the form of a packet from the server
    availableTeams.push_back("Fascist");
    availableTeams.push_back("Communist");
    availableTeams.push_back("Feminist");

    for(const auto &teamName : availableTeams) {
        teamLogos.push_back(Game::device->getVideoDriver()->createImageFromFile(std::string(cpplocate::findModule("dmux").value("logoDir") + teamName + ".png").c_str()));
    }

    for(const auto &logo : teamLogos) {
        logoTextures.push_back(*pGUI->createTexture(logo));
        logo->drop();
    }

    metaData = core::sys::getSceneMetaDataFromDir(cpplocate::findModule("dmux").value("sceneDir"));

}

Menu::~Menu() {
    for(const auto &logo : logoTextures) {
        pGUI->deleteTexture(&logo.get());
    }
}

void Menu::showFindGame() {

    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
    ImGui::SetNextWindowSize(ImVec2(screenWidth(1),
                                    screenHeight(0.9f)));

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    ImGui::Begin("Find a game to play", nullptr, ImGuiWindowFlags_NoTitleBar);
    ImGui::PushFont(font["regular"]);
    ImGui::PushStyleVar(ImGuiStyleVar_ChildWindowRounding, 5.0f);
    ImGui::BeginChild("Sub2", ImVec2(0, screenHeight(0.8f)), true);
    ImGui::Text("Available Maps");
    ImGui::Columns(2);

    std::vector<std::string> arenaNames;
    for(auto const &arena : metaData) { //through the vector
        for(auto const &z : arena) { //through the map inside the vector
            if(z.first == "Name") {
                arenaNames.push_back(z.second);
            }
        }
    }

    std::vector<std::string> serverNames;
    std::vector<std::string> serverIp;
    for(auto &server : NetCommon::serverList.data) {
        serverNames.push_back(server.second.serverName);
        serverIp.push_back(server.first);
    }

    ImGui::ListBox("Native Maps", &arenaSelection, toSharedPointer(arenaNames).get(), arenaNames.size());

    ImGui::NextColumn();

    ImGui::Text("%s", std::string("Artist    " + metaData[arenaSelection]["Artist"]).c_str());

    ImGui::Text("Available Servers:");
    ImGui::Columns(4, "mycolumns"); // 4-ways, with border
    ImGui::Separator();
    ImGui::Text("Name");
    ImGui::NextColumn();
    ImGui::Text("Loaded Map");
    ImGui::NextColumn();
    ImGui::Text("Game Mode");
    ImGui::NextColumn();
    ImGui::Text("IP Address");
    ImGui::NextColumn();
    ImGui::Separator();
    {
        int i = 0;
        for(auto &server : NetCommon::serverList.data) {
            if(ImGui::Selectable(server.second.serverName.c_str(), serverSelection == i, ImGuiSelectableFlags_SpanAllColumns)) {
                serverSelection = i;
            }
            ImGui::NextColumn();
            ImGui::Text("%s", server.second.arenaName.c_str());
            ImGui::NextColumn();
            ImGui::Text("%s", std::to_string(server.second.gameMode).c_str());
            ImGui::NextColumn();
            ImGui::Text("%s", std::string(RakNet::SystemAddress(server.first.c_str()).ToString(false) + std::string(":") + std::to_string(server.second.port)).c_str());
            ImGui::NextColumn();
            ++i;
        }
    }
    ImGui::Columns(1);
    ImGui::Separator();

    ImGui::EndChild();
    ImGui::PopStyleVar();

    if(ImGui::Button("Start Game", ImVec2(screenWidth(0.3f), 50))) {
        currentServer = RakNet::SystemAddress(serverIp[serverSelection].c_str());
        currentServer.SetPortHostOrder(NetCommon::serverList.data[serverIp[serverSelection].c_str()].port);
        closeMenu(); // This will start the game
    }
    ImGui::SameLine();
    ImGui::InputText("IP", customServer.data(), customServer.size());
    ImGui::InputText("Port", customIp.data(), customIp.size());
    if(ImGui::Button("Custom Connect")) {
        currentServer = RakNet::SystemAddress(customServer.data());
        currentServer.SetPortHostOrder(std::stoi(customIp.data()));
        closeMenu();
    }
    ImGui::PopFont();
    ImGui::End();
    ImGui::PopStyleVar();
}

RakNet::SystemAddress Menu::getCurrentServer() {
    return currentServer;
}

bool Menu::showTeamSelection() {

    ImGui::SetNextWindowPos(ImVec2(screenWidth(0.25f), screenHeight(0.25f)));
    ImGui::SetNextWindowSize(ImVec2(screenWidth(0.5f), screenHeight(0.5f) / 2));

    ImGui::Begin("Team Selection", nullptr, ImGuiWindowFlags_NoTitleBar);

    ImGui::BeginGroup();
    ImGui::Text("Available Teams");
    for(const auto &logo : logoTextures) {
        ImGui::Image(logo.get(), ImVec2((screenWidth(0.5f) / 3) - 10, screenHeight(0.5f) / 3));
        ImGui::SameLine();
    }
    ImGui::EndGroup();

    ImGui::BeginGroup();
    for(const auto &team : availableTeams) {
        if(ImGui::Button(team.c_str(), ImVec2((screenWidth(0.5f) / 3) - 10, 50.0f))) {
            JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
            ImGui::EndGroup();
            ImGui::End();
            Game::device->getCursorControl()->setVisible(false); // in case it is the first selection

            return true; //team has been selected
        }
        ImGui::SameLine();
    }
    ImGui::EndGroup();

    ImGui::End();
    return false; //team has not been selected
}

bool Menu::showPauseMenu() {

    ImGui::SetNextWindowPos(ImVec2(screenWidth(0.25f), screenHeight(0.25f)));
    ImGui::SetNextWindowSize(ImVec2(screenWidth(0.5f), screenHeight(0.5f)));

    ImGui::Begin("Pause Menu", nullptr, ImGuiWindowFlags_NoTitleBar);
    for(const auto &text : pauseMenuOptions) {
        if(ImGui::Button(text.c_str(), ImVec2(200, 40))) {
            if(text == std::string("Return to game")) {
                Game::device->getCursorControl()->setVisible(false);
                ImGui::End();
                return false; //no longer in pause menu
            } else if(text == std::string("Change Team Alignment")) {
                selectedWindow = 1;
            } else if(text == std::string("Settings")) {
                selectedWindow = 2;
            } else if(text == std::string("Leave game")) {

            } else if(text == std::string("Exit")) {
                Game::device->closeDevice();
            }
        }
    }
    ImGui::End();

    switch(selectedWindow) {
        case 0:
            closeMenu();
            break;
        case 1:
            Game::device->getCursorControl()->setVisible(true);
            if(showTeamSelection()) {
                selectedWindow = 10; //no selected window
            }
            break;
        case 2:
            showSettings();
            break;
        case 3:
            Game::device->closeDevice();
            break;
    }
    return true; //still in menu
}

std::string &Menu::showMainMenu() {

    pGUI->startGUI();


    showNavBar();

    switch(selectedWindow) {
        case 0:
            showFindGame();
            break;
        case 1:
            garage.show();
            break;
        case 2:
            showSettings();
            break;
        case 3:
            showCredits();
            break;
        case 4:
            Game::device->closeDevice();
            break;
    }

    Game::device->getSceneManager()->drawAll();
    pGUI->drawAll();

    return metaData[arenaSelection]["objStr"];
}

void Menu::showNavBar() {

    ImGui::SetNextWindowPos(ImVec2(0, screenHeight(0.9f)));
    ImGui::SetNextWindowSize(ImVec2(screenWidth(1), screenHeight(0.1f)));

    // This should be replaced with a menu bar some day
    // Dan Gibson - The way we were

    // Make window background transparent
    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
    ImGui::Begin("Main Menu", nullptr, ImGuiWindowFlags_NoTitleBar);    
    ImGui::PushFont(font["heavy"]);
    ImGui::BeginGroup();
    for(unsigned int i = 0; i < availableButtons.size(); ++i) {
        if(ImGui::Button(availableButtons[i].c_str(), ImVec2(ImGui::GetWindowWidth() / availableButtons.size(),
                                                             ImGui::GetWindowHeight()))) {
            JukeBox::playFX("metal_interactions/metal_interaction1.ogg");
            selectedWindow = i;
        }
        ImGui::SameLine();
    }
    ImGui::EndGroup();
    ImGui::PopFont();
    ImGui::End();
    ImGui::PopStyleVar();
    ImGui::PopStyleColor();
}

bool Menu::OnEvent(const irr::SEvent &event) {

    switch(event.EventType) {
        case irr::EET_KEY_INPUT_EVENT:
            if(isKeyModifying) {
                Game::playerSettings.set.controls[controlMod] = std::to_string(event.KeyInput.Key);
                isKeyModifying = false;
            }
            keyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
            break;
        default:
            break;
    }

    return false;
}

void modKey(const char *label);
void keyInputMenu();
void saveSettingsXML();

void modKey(const char *label) {
    if(isKeyModifying) {
        ImGui::Text("%s", std::string("Press key for ").append(label).c_str());
    }
}

void Menu::videoSettingsInputMenu() {

    if(isVideoSettingsMenuActive) {
        ImGui::Begin("Video Settings", nullptr, ImGuiWindowFlags_NoTitleBar);
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::BeginGroup();

        for(auto &box : Game::playerSettings.set.toggle) {
            ImGui::Checkbox(box.first.c_str(), &box.second);
        }

        ImGui::Text("Resolutions");

        for(unsigned int i = 0; i < Game::playerSettings.set.supportedResolutions.size(); ++i) {
            std::string text = (std::to_string(Game::playerSettings.set.supportedResolutions[i].first) + " x " + std::to_string(Game::playerSettings.set.supportedResolutions[i].second));

            if((Game::playerSettings.set.supportedResolutions[i].first == Game::playerSettings.set.supportedResolutions[Game::playerSettings.set.selectedResolution].first) and
                    (Game::playerSettings.set.supportedResolutions[i].second == Game::playerSettings.set.supportedResolutions[Game::playerSettings.set.selectedResolution].second)) {
                if(ImGui::RadioButton(text.c_str(), true)) {
                    Game::playerSettings.set.selectedResolution = i;
                }
            } else {
                if(ImGui::RadioButton(text.c_str(), false)) {
                    Game::playerSettings.set.selectedResolution = i;
                }
            }
        }

        if(ImGui::Button("Okay", ImVec2(screenWidth(0.25f), screenWidth(0.05f)))) {
            isVideoSettingsMenuActive = false;
        }

        ImGui::EndGroup();
        ImGui::End();
    }
}

void Menu::keyInputMenu() {

    // Make this a modal
    if(isKeyInputMenuActive) {
        ImGui::Begin("Controls", nullptr, ImGuiWindowFlags_NoTitleBar);
        ImGui::SameLine(0.0f, 10.0f);
        ImGui::BeginGroup();
        // TODO: Change the mouseSensitivity from a slider to an input field.
        if(ImGui::SliderFloat("Mouse sensitivity", &Game::playerSettings.set.mouseSensitivity, irr::u32(1), irr::u32(100))) {
            JukeBox::playFX("metal_interactions/metal_swing1.ogg");
            Game::playerSettings.saveSettings();
        }

        for(auto const &name : Game::playerSettings.set.controls) {
            ImGui::BeginGroup();
            if(ImGui::Button(name.first.c_str(), ImVec2(screenWidth(0.25f), screenWidth(0.05f)))) {
                JukeBox::playFX("metal_interactions/metal_button_press2.ogg");
                isKeyModifying = true;
                controlMod = name.first;
                Game::playerSettings.saveSettings();
            }
            ImGui::SameLine();
            ImGui::Text("%s", name.second.c_str());
            ImGui::EndGroup();
        }

        if(ImGui::Button("Okay", ImVec2(screenWidth(0.25f), screenHeight(0.05f)))) {
            JukeBox::playFX("metal_interactions/metal_interaction1.ogg");
            isKeyInputMenuActive = false;
        }

        //If the user is modifying a key input
        modKey(controlMod.c_str());
        ImGui::EndGroup();
        ImGui::End();
    }
}

void Menu::showSettings() {

    // create second window with some control inputs
    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
    ImGui::SetNextWindowSize(ImVec2(screenWidth(1),
                                    screenHeight(0.9f)));

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    ImGui::Begin("Player Settings", nullptr, ImGuiWindowFlags_NoTitleBar);
    ImGui::PushFont(font["regular"]);
    ImGui::SameLine(0.0f, 10.0f);
    ImGui::BeginGroup();


    ImGui::PushItemWidth(screenWidth(0.8f));
    if(ImGui::SliderInt("Master Volume", &Game::playerSettings.set.volumes["Master"], irr::u32(0), irr::u32(100))) {
        Game::playerSettings.saveSettings();
    }

    if(ImGui::SliderInt("SoundFX Volume", &Game::playerSettings.set.volumes["SoundFX"], irr::u32(0), irr::u32(100))) {
        Game::playerSettings.saveSettings();
    }

    if(ImGui::SliderInt("Music Volume", &Game::playerSettings.set.volumes["Music"], irr::u32(0), irr::u32(100))) {
        Game::playerSettings.saveSettings();
    }
    ImGui::PopItemWidth();

    ImGui::PushItemWidth(screenWidth(0.5f));
    ImGui::InputText("Alias: ", Game::playerSettings.set.alias.data(), 32);
    ImGui::PopItemWidth();

    if(ImGui::Button("Controls", ImVec2(screenWidth(0.25f), screenWidth(0.05f)))) {
        JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
        if(not isKeyInputMenuActive) {
            ImGui::SetNextWindowPos(ImVec2(screenWidth(0.25f), screenHeight(0.25f)));
            ImGui::SetNextWindowSize(ImVec2(screenWidth(0.5f), screenHeight(0.5f)));
        }
        isKeyInputMenuActive = true;
    }

    if(ImGui::Button("Video Settings", ImVec2(screenWidth(0.25f), screenHeight(0.05f)))) {
        JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
        ImGui::SetNextWindowPos(ImVec2(screenWidth(0.25f), screenHeight(0.25f)));
        ImGui::SetNextWindowSize(ImVec2(screenWidth(0.5f), screenHeight(0.5f)));
        isVideoSettingsMenuActive = true;
    }

    if(ImGui::Button("Apply", ImVec2(screenWidth(0.25f), screenHeight(0.05f)))) {
        JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
        Game::playerSettings.saveSettings();
    }

    if(ImGui::Button("Okay", ImVec2(screenWidth(0.25f), screenHeight(0.05f)))) {
        JukeBox::playFX("metal_interactions/metal_interaction2.ogg");
        selectedWindow = 10; //make the settings window inactive
    }

    videoSettingsInputMenu();
    keyInputMenu();
    ImGui::EndGroup();
    ImGui::PopFont();
    ImGui::End();
    ImGui::PopStyleVar();
}

void Menu::showCredits() {

    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
    ImGui::SetNextWindowSize(ImVec2(screenWidth(1),
                                    screenHeight(0.9f)));

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    ImGui::Begin("Credits", nullptr, ImGuiWindowFlags_NoTitleBar);
    ImGui::PushFont(font["regular"]);
    ImGui::BeginGroup();

    for(const auto &text : contributorFileContent) {
        ImGui::TextWrapped(text.c_str());
    }

    ImGui::EndGroup();
    ImGui::SameLine(0.0f, 10.0f);
    ImGui::PopFont();
    ImGui::End();
    ImGui::PopStyleVar();
}
}
