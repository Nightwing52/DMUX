#include <iostream>
#include <map>
#include <vector>
#include <fstream>
#include <easylogging++.h>
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/array.hpp>

#include "client/Game.hpp"
#include "client/JukeBox.hpp"
#include "Settings.hpp"
#include "DmuxCommon.hpp"

std::string controlMod;

SettingsData::SettingsData() {
    //defaults . . . will get erased if settings file exists
    controls["Forward"] = "w";
    controls["Backward"] = "s";
    controls["Right"] = "d";
    controls["Left"] = "a";
    controls["Jump"] = "j";
    controls["Crouch"] = "c";
    controls["Sprint"] = "q" ;

    controls["Attack"] = "e";
    controls["Alt-Attack"] = "t";
    controls["Reload"] = "r";
    controls["Aim"] = "y";

    volumes["Master"]  = 0;
    volumes["Music"]   = 0;
    volumes["SoundFX"] = 0;

    toggle["FullScreen"] = false;
    toggle["v-sync"] = false;
    toggle["Anti-Aliasing"] = false;

    supportedResolutions.push_back(std::make_pair(800, 600));
    supportedResolutions.push_back(std::make_pair(1024, 768));
    supportedResolutions.push_back(std::make_pair(1280, 800));
    supportedResolutions.push_back(std::make_pair(1366, 768));
    supportedResolutions.push_back(std::make_pair(1280, 1024));
    supportedResolutions.push_back(std::make_pair(1440, 900));
    supportedResolutions.push_back(std::make_pair(1680, 1050));
    supportedResolutions.push_back(std::make_pair(1920, 1080));
    supportedResolutions.push_back(std::make_pair(1600, 900));
    //supportedResolutions.push_back(std::make_pair(320, 480));

    currentWindowSize.first  = 800;
    currentWindowSize.second = 600;

    {
        std::string defAlias("Unnamed");
        std::copy(defAlias.begin(), defAlias.end(), alias.data());
    }

    mouseSensitivity = 50;
}

Settings::Settings() :
    set(),
    fileName(cpplocate::findModule("dmux").value("confDir") + std::string("Dmux_Client.conf")) {

    //Overwrite these settings if we have a save file
    readSettings();

    for(unsigned int i = 0; i < set.supportedResolutions.size(); ++i) {
        if(set.supportedResolutions[i] == set.currentWindowSize) {
            set.selectedResolution = i;
        }
    }
}

irr::f32 Settings::getMasterVolume() {
    return set.volumes["Master"];
}

irr::f32 Settings::getMusicVolume() {
    return set.volumes["Music"];
}

irr::f32 Settings::getSoundVolume() {
    return set.volumes["SoundFX"];
}

void Settings::saveSettings() {

    set.currentWindowSize = set.supportedResolutions[set.selectedResolution];
    // Now save the adjusted layout to the file
    std::ofstream outputStream(fileName, std::ios::binary);
    cereal::PortableBinaryOutputArchive archive(outputStream);
    archive(set);
}

irr::SIrrlichtCreationParameters Settings::getIrrlichtParams() {

    bool vSync = false;
    bool antiAlias = true;
    bool fullScreen = false;

    for(auto &option : set.toggle) {
        if(std::string("v-sync") == option.first) {
            vSync = option.second;
        }

        if(std::string("Anti-Aliasing") == option.first) {
            antiAlias = option.second;
        }

        if(std::string("FullScreen") == option.first) {
            fullScreen = option.second;
        }
    }

    irr::SIrrlichtCreationParameters IrrlichtParams;
    IrrlichtParams.DriverType    = irr::video::EDT_OPENGL;
    IrrlichtParams.WindowSize    = irr::core::dimension2d<irr::u32>(set.currentWindowSize.first, set.currentWindowSize.second);
    IrrlichtParams.Bits          = 32;
    IrrlichtParams.Fullscreen    = fullScreen;
    IrrlichtParams.Stencilbuffer = true;

    if(antiAlias) {
        IrrlichtParams.AntiAlias     = 16;
    } else {
        IrrlichtParams.AntiAlias     = 0;
    }

    IrrlichtParams.Vsync         = vSync;
    IrrlichtParams.EventReceiver = 0;

    return IrrlichtParams;
}
void Settings::readSettings() {

    // Now save the adjusted layout to the file
    std::ifstream inputStream(fileName, std::ios::binary);
    if(inputStream.good()) {
        cereal::PortableBinaryInputArchive archive(inputStream);
        archive(set);
    } else {
        LOG(WARNING) << "settings file does not exist, making a new one";
    }
}
