#pragma once

/**
 * @file   Settings.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 */

#include <irrTypes.h>
#include <vector>
#include <map>

// Please get rid of these
extern std::map<const std::string, std::string> controls;
extern bool isKeyModifying;
extern std::string controlMod;

/**
 * \brief contains all the entries for user settings that get serialized
 */
struct SettingsData final {
    SettingsData();
    std::map<std::string, int> volumes; //!< Volumes defined by the user
    std::map<std::string, bool> toggle; //!< All toggleable settings
    std::map<std::string, std::string> controls; //!< Mappable controls
    std::vector<std::pair<irr::u32, irr::u32>> supportedResolutions; //!< All resolutions supported by DMUX
    std::pair<irr::u32, irr::u32> currentWindowSize; //!< Size of current window
    irr::f32 mouseSensitivity; //!< Mouse sensitivity
    irr::u32 selectedResolution; //!< resolution the player has selected
    std::array<char, 32> alias; //!< Player alias

    template <class Archive>
    void serialize(Archive &ar) {
        ar(volumes, toggle, controls, supportedResolutions, currentWindowSize, mouseSensitivity, selectedResolution, alias);
    }
};

/**
 * \brief Container class for SettingsData, really badly done
 * \details Please refactor this class before adding anything additional to it. The set variable should eventually be made private and replaced with accessor functions such as getAttribute().
 */
class Settings final {
public:
    Settings();
    irr::SIrrlichtCreationParameters getIrrlichtParams();
    void saveSettings(); //!< Cereal will serialize the settings
    irr::f32 getMasterVolume(); //!< Gets master volume as defined by player
    irr::f32 getSoundVolume(); //!< Gets sound effects volume as defined by player
    irr::f32 getMusicVolume(); //!< Gets music volume as defined by player
    SettingsData set; //!< Holds the actual data for the settings. We want to make this private.

private:
    std::string fileName; //!< file path for the settings
    void readSettings(); //!< Deserializes the settings file
    void *operator new(size_t);     //!< Preventing heap allocation
    void *operator new[](size_t);   //!< Preventing heap allocation
    void  operator delete(void *) {}    //!< Preventing heap allocation
    void  operator delete[](void *) {} //!< Preventing heap allocation
};
