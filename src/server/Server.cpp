#include <iostream>
#include <RakSleep.h>
#include <cereal/archives/json.hpp>
#include <MessageIdentifiers.h>

#include "Server.hpp"
#include "DmuxCommon.hpp"
#include "sys/XmlParser.hpp"

Server::Server() :
    NetCommon(),
    settings(),
    serverPort("12347"),
    masterServerPort("16722"),
    ip("192.168.1.123"),
    socketDescriptor(std::stoi(serverPort), 0) { //binding the server to its port
    socketDescriptor.socketFamily = AF_INET;

    settings.serverName = "Default Server";
    settings.arenaName  = "olivermath";
    settings.gameMode   = 0;
    settings.port = std::stoi(serverPort);

    if(fileExists(cpplocate::findModule("dmux").value("confDir") + "server.conf")) {
        std::ifstream is(cpplocate::findModule("dmux").value("confDir") + "server.conf");
        {
            cereal::JSONInputArchive Archive(is);
            Archive(settings);
        }
    }

    std::ofstream os(cpplocate::findModule("dmux").value("confDir") + "server.conf");
    cereal::JSONOutputArchive archive(os);
    archive(settings);

    // From now on we can imply the settings file is valid

    LOG(INFO) << settings.port;
    std::vector<std::map<std::string, std::string>> availableArenas = core::sys::getSceneMetaDataFromDir(cpplocate::findModule("dmux").value("sceneDir"));
    // Getting the currently set map
    for(auto arena : availableArenas) {
        if(arena["objStr"] == settings.arenaName) {
            LOG(INFO) << arena["objStr"];
            currentArena = arena;
        }
    }

    interface["client"] = RakNet::RakPeerInterface::GetInstance();
    interface["client"]->SetIncomingPassword(password.c_str(), password.length());
    interface["client"]->SetTimeoutTime(3000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);

    interface["client"]->SetUnreliableTimeout(1000);

    RakNet::SocketDescriptor socketdesc;
    socketdesc.port = settings.port;
    socketdesc.socketFamily = AF_INET; // Test out IPV4

    {
        bool b = interface["client"]->Startup(1024, &socketdesc, 1) == RakNet::RAKNET_STARTED;
        interface["client"]->SetMaximumIncomingConnections(4);
        if(!b) {
            LOG(WARNING) << "Failed to start dual IPV4 and IPV6 ports. Trying IPV4 only.";

            // Try again, but leave out IPV6
            b = interface["client"]->Startup(4, &socketdesc, 1) == RakNet::RAKNET_STARTED;
            if(!b) {
                LOG(ERROR) << "Interface failed to start.  Terminating.";
                exit(1);
            }
        }
    }

    interface["master"] = RakNet::RakPeerInterface::GetInstance();
    interface["master"]->SetOccasionalPing(true);

    RakNet::SocketDescriptor desc(RakNet::UNASSIGNED_SYSTEM_ADDRESS.GetPort(), 0);
    desc.socketFamily = AF_INET;

    // Connecting the client is very simple.  0 means we don't care about
    // a connectionValidationInteger, and false for low priority threads
    bool b = interface["master"]->Startup(16, &desc, 1) == RakNet::RAKNET_STARTED;
    if(!b) {
        LOG(ERROR) << "Failed to start IPV4 ports. Time to kill yourself.";
    } else {
        LOG(INFO) << "The master server socket has been successfully bound.";
    }
    interface["master"]->SetMaximumIncomingConnections(4);
    interface["master"]->Connect(ip.c_str(), std::stoi(masterServerPort), password.c_str(), password.length());

    LOG(INFO) << "DMUX Server is starting! Resources have been initialized";
}

int Server::operator()() {

    // Loop for input
    while(true) {
        RakSleep(10);
        recievePacketHandle();
    }
    return 0;
}

void Server::printIpAddresses() {
    LOG(INFO) << "My IP addresses:";
    unsigned int i;
    for(i = 0; i < interface["master"]->GetNumberOfAddresses(); i++) {
        LOG(INFO) << i + 1 << ". " << interface["master"]->GetLocalIP(i);
    }
}

void Server::recievePacketHandle() {

    for(packet = interface["client"]->Receive(); packet; interface["client"]->DeallocatePacket(packet), packet = interface["client"]->Receive()) {

        LOG(INFO) << "Got packet";
        std::cout << static_cast<unsigned short>(packet->data[0]) << std::endl;
        switch(static_cast<unsigned char>(packet->data[0])) {
        case ID_CONNECTION_REQUEST:
                // This tells the client they have connected
                LOG(INFO) << "Client connected with address: " << packet->systemAddress.ToString(false);
                break;
        case ID_DISCONNECTION_NOTIFICATION:
            LOG(ERROR) << "Client with address: " << packet->systemAddress.ToString(false) << "disconnected.";
            break;
        }
    }

    // Get a packet from either the server or the interface
    for(packet = interface["master"]->Receive();
        packet;
        interface["master"]->DeallocatePacket(packet), packet = interface["master"]->Receive()) {
        // We got a packet, get the identifier with our handy function

        if(packet->data[0] == 140) {
            if(packet->data[1] == 16) {
                LOG(INFO) << "Requesting server list";
                serverList = deserializePacket<GameServerList>(packet);
            }
        }

        // Check if this is a network message packet
        switch(packet->data[0]) {
            case ID_DISCONNECTION_NOTIFICATION:
                // Connection lost normally
                LOG(ERROR) << "Lost connection to master server.";
                break;
            case ID_ALREADY_CONNECTED:
                // Connection lost normally
                LOG(WARNING) << "The client is already connected to the master server.";
                break;
            case ID_INCOMPATIBLE_PROTOCOL_VERSION:
                LOG(ERROR) << "Incompatible protocol.";
                break;
            case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
                LOG(INFO) << "ID_REMOTE_DISCONNECTION_NOTIFICATION";
                break;
            case ID_REMOTE_CONNECTION_LOST: // Server telling the clients of another client disconnecting forcefully.  You can manually broadcast this in a peer to peer enviroment if you want.
                LOG(INFO) << "ID_REMOTE_CONNECTION_LOST";
                break;
            case ID_REMOTE_NEW_INCOMING_CONNECTION: // Server telling the clients of another client connecting.  You can manually broadcast this in a peer to peer enviroment if you want.
                LOG(INFO) << "ID_REMOTE_NEW_INCOMING_CONNECTION";
                break;
            case ID_CONNECTION_BANNED: // Banned from this server
                LOG(ERROR) << "We are banned from this server.";
                break;
            case ID_CONNECTION_ATTEMPT_FAILED:
                LOG(ERROR) << "Connection attempt failed";
                break;
            case ID_NO_FREE_INCOMING_CONNECTIONS:
                // Sorry, the server is full.  I don't do anything here but
                // A real app should tell the user
                LOG(ERROR) << "ID_NO_FREE_INCOMING_CONNECTIONS";
                break;

            case ID_INVALID_PASSWORD:
                LOG(ERROR) << "ID_INVALID_PASSWORD";
                break;

            case ID_CONNECTION_LOST:
                // Couldn't deliver a reliable packet - i.e. the other system was abnormally
                // terminated
                LOG(ERROR) << "ID_CONNECTION_LOST";
                break;

            case ID_CONNECTION_REQUEST_ACCEPTED:
                // This tells the client they have connected
                LOG(INFO) << "ID_CONNECTION_REQUEST_ACCEPTED to " << packet->systemAddress.ToString(true) << " with GUID " << packet->guid.ToString();
                //                LOG(INFO) << "My external address to the master server is " << interface["master"]->GetExternalID(packet->systemAddress).ToString(true);
                sendPacket(interface["master"], ip.c_str(), settings, 140, 14, true); // We send the master server our game server rules
                sendPacket(interface["master"], ip.c_str(), 0, 140, 15, true); // 0 for an empty packet
                break;
            case ID_CONNECTED_PING:
            case ID_UNCONNECTED_PING:
                LOG(INFO) << "Ping from " << packet->systemAddress.ToString(true);
                break;
            default:
                break;
        }
    }
}
