#pragma once

/**
 * @file   Server.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Main class for game server, connects to master server and game client
 * upon client request
 */

#include "NetCommon.hpp"

/**
 * \brief Main class for game server
 */
class Server final : public NetCommon {
public:
    Server();
    int operator()(); //!< Main loop of Server

private:
    void recievePacketHandle(); //!< Retrieves packets from the network buffer
    GameServer settings; //!< Configuration of the server
    std::string serverPort; //!< Port the server is bound to
    std::string masterServerPort; //!< Port master server is running on
    std::string ip; //!< IP of the master server
    RakNet::SocketDescriptor socketDescriptor; //!< Which sockets the server should use
    void printIpAddresses(); //!< Logs currently connected IP addresses
    std::map<std::string, std::string> currentArena; //!< Information about the currently loaded arena
};
