#pragma once

/**
 * @file   Graphics.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @brief  Graphics system for Irrlicht
 */
#include "comp/Graphics.hpp"

namespace core {
namespace sys {
    //! Adds the graphics module to the scene
void addNode(comp::Graphics &graphics);
} // sys
} // core
