#include <BulletCollision/Gimpact/btGImpactShape.h>
#include <BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>
#include <cassert>
#include "sys/Physics.hpp"
#include "Simulation.hpp"

namespace core {
namespace sys {
void addToWorld(comp::Physics &physics) {
    physics.transform.setIdentity();
    physics.transform.setOrigin(physics.position);
    physics.motionState = new btDefaultMotionState(physics.transform);

#ifndef NDEBUG
    assert(physics.shape not_eq nullptr);
    assert(physics.motionState not_eq nullptr);
#endif
    static_cast<btGImpactMeshShape *>(physics.shape)->updateBound();

    // Add mass and calculate local inertia
    physics.shape->calculateLocalInertia(physics.mass, physics.localInertia);

    btRigidBody::btRigidBodyConstructionInfo info(physics.mass, physics.motionState, physics.shape, physics.localInertia);
    // Create the rigid body object
    physics.rigidBody = new btRigidBody(info);
#ifndef NDEBUG
    assert(physics.motionState not_eq nullptr);
    assert(physics.shape not_eq nullptr);
    assert(physics.localInertia not_eq nullptr);
    assert(physics.rigidBody not_eq nullptr);
#endif


#ifndef NDEBUG
    assert(physics.shape not_eq nullptr);
    assert(physics.rigidBody not_eq nullptr);
#endif
    Simulation::mir.collisionShapes.push_back(physics.shape);
    Simulation::mir.world->addRigidBody(physics.rigidBody);
    Simulation::mir.worldObjs.push_back(physics.rigidBody);
}
} // sys
} // core
