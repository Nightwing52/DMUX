#pragma once

/**
 * @file   Physics.hpp
 * @author Brigham Keys, Esq. (bkeys@bkeys.org)
 * @author Nicolas Ortega (deathsbreed@themusicinnoise.net)
 * @brief  system for handling bullet physics
 *
 * Copyright (c) 2016 Collective Tyranny
 */

#include <LinearMath/btMotionState.h>
namespace core {
namespace comp {
class Physics;
} // comp
} // core
namespace core {
namespace sys {
void addToWorld(comp::Physics &physics);
} // sys
} // core
