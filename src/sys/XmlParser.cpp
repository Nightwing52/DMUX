#include <easylogging++.h>
#include <cpplocate/ModuleInfo.h>
#include <cpplocate/cpplocate.h>
#include <tinyxml2.h>
#include <LinearMath/btVector3.h>
#include <iostream>

#include "XmlParser.hpp"
#include "DmuxCommon.hpp"

namespace core {
namespace sys {

// Private XML parsing functions
void initXMLDoc(tinyxml2::XMLDocument &doc, const std::string &fileName);
std::map<std::string, std::string> getMetaDataFromScene(const std::string &fileName);
std::map<std::string, btVector3> getWheelData(const std::string &fileName);

std::map<std::string, btVector3> getWheelData(const std::string &fileName) {

    std::map<std::string, btVector3> wheelPositions;
    tinyxml2::XMLDocument doc;
    initXMLDoc(doc, fileName);

    tinyxml2::XMLElement *root = doc.FirstChildElement("scene");
    tinyxml2::XMLElement *nodes = root->FirstChildElement("nodes");

    std::string nodeName;
    for(tinyxml2::XMLElement *e = nodes->FirstChildElement("node"); e not_eq nullptr;
            e = e->NextSiblingElement("node")) {
        nodeName = e->Attribute("name");

        // If the node contains wheel data
        if(nodeName.find("-wheel") not_eq std::string::npos) {
            tinyxml2::XMLElement *wheelPosition = e->FirstChildElement("position");
            wheelPositions[nodeName] = btVector3(wheelPosition->FloatAttribute("x"),
                                                 wheelPosition->FloatAttribute("y"),
                                                 wheelPosition->FloatAttribute("z"));
        }
    }
    assert(wheelPositions.size() > 0);
    return wheelPositions;
}

std::map<std::string, std::map<std::string, btVector3>> getWheelPositions() {

    std::map<std::string, std::map<std::string, btVector3>> wheelPositions;

    processDirectories(cpplocate::findModule("dmux").value("chassisDir"),
                       ".scene",
    [&](std::string dirName, std::string fileToInspect) {
        wheelPositions[dirName] = getWheelData(fileToInspect);
    });

    return wheelPositions;
}

void initXMLDoc(tinyxml2::XMLDocument &doc, const std::string &fileName) {

    try {
        tinyxml2::XMLError eResult =
            doc.LoadFile(fileName.c_str());

        if(eResult not_eq tinyxml2::XML_SUCCESS) {
            throw std::string("Error: XML file does not exist or is corrupt (like Hillary Clinton)");
        }
    } catch(const std::string e) {
        doc.PrintError();
        LOG(ERROR) << e;
        LOG(ERROR) << "DMUX is going to have to make an educated guess at where your tires will connect, where the vehicle's weapon gets mounted, etc. This error WILL NOT be solved by restarting the game.";
        LOG(ERROR) << "If you can replicate this error please email the DMUX developers at bkeys@gnu.org with the error message, your operating system, and version of DMUX you have installed";
        return;
    }
}

std::map<std::string, std::string> getMetaDataFromScene(const std::string &fileName) {
    std::map<std::string, std::string> sceneMetaData;
    tinyxml2::XMLDocument doc;
    initXMLDoc(doc, fileName);

    tinyxml2::XMLElement *root = doc.FirstChildElement("scene");
    tinyxml2::XMLElement *nodes = root->FirstChildElement("nodes");

    std::string nodeName;
    for(tinyxml2::XMLElement *e = nodes->FirstChildElement("node"); e not_eq nullptr;
            e = e->NextSiblingElement("node")) {
        nodeName = e->Attribute("name");
        std::string mystr = nodeName.substr(0, nodeName.find(".", 0));

        if(nodeName == "MetaData") {
            for(tinyxml2::XMLElement *userData = e->FirstChildElement("user_data");
                    userData not_eq nullptr;
                    userData = userData->NextSiblingElement("user_data")) {
                sceneMetaData[userData->FirstAttribute()->Value()] = userData->Attribute("value");
            }
        }
    }
    return sceneMetaData;
}

std::vector<std::map<std::string, std::string>> getSceneMetaDataFromDir(const std::string &containingDir) {
    std::vector<std::map<std::string, std::string>> metaDataContainer;

    processDirectories(containingDir, ".scene", [&](std::string dirName, std::string fileToInspect) {
        metaDataContainer.push_back(core::sys::getMetaDataFromScene(fileToInspect));
        metaDataContainer.back()["objStr"] = dirName;
    });

    return metaDataContainer;
}

}
}
