#pragma once

#include <memory>
#include <string>
#include <map>
#include <vector>

class btVector3;

namespace core {
namespace sys {
std::vector<std::map<std::string, std::string>> getSceneMetaDataFromDir(const std::string &containingDir);
std::map<std::string, std::map<std::string, btVector3>> getWheelPositions();
}
}
